const Config = require('config');
const Console = require('console');
const Bcrypt = require('bcrypt');
const Readline = require('read');
const Joi = require('joi');
const Yargs = require('yargs');
const Op = require('sequelize').Op;
const DBManager = require('./utils/database').Manager;
const { User } = require('./utils/database').Schema;

const promptPassword = (callback) => {
  Readline( { prompt: 'Enter Password: ', silent: true }, (err1, password) => {
    if (err1) {
      callback(err1);
    } else {
      Readline( { prompt: 'Verify Password: ', silent: true }, (err2, verify) => {
        if (err2) {
          callback(err2);
        } else {
          if (password === verify) {
            const result = Joi.validate(password, Joi.string().min(Config.security.minPasswordLength));
            if (result.error === null) {
              Bcrypt.hash(password, Config.security.hashSaltRounds, (err, hash) => {
                if (err) {
                  callback(err);
                } else {
                  callback(null, hash);
                }
              });
            } else {
              Console.error(`Password length must be at least ${Config.security.minPasswordLength}. Try again.`);
              promptPassword(callback);
            }
          } else {
            // Repeat
            Console.error('Password mismatch. Try again.');
            promptPassword(callback);
          }
        }
      });
    }
  });
};

const inputPassword = () => {
  return new Promise((resolve, reject) => {
    promptPassword((err, password) => {
      if (err) {
        reject(err);
      }

      resolve(password);
    });
  });
};

const addUserCommand = {
  command: 'adduser',
  desc: `Create new user/admin.`,
  builder: (yargs) => {
    yargs.string('user')
      .describe('user', 'Set Username.')
      .alias('user', 'u')
      .implies('user', 'email')
      .string('email')
      .describe('email', 'Set Email.')
      .alias('email', 'e')
      .demandOption('user', 'email')
      .boolean('admin')
      .describe('admin', 'Set Role as admin.')
      .default('admin', 'false')
      .alias('admin', 'a');
  },
  handler: async (argv) => {
    let result = Joi.validate(argv.user, Joi.string().regex(/^[a-zA-Z0-9]+(?:[-_.]?[a-zA-Z0-9]+)*$/));
    if (result.error !== null) {
      console.error('Username must only contains alphanumeric, dash(-), underscore(-) or dot(.)');
      return;
    }

    result = Joi.validate(argv.email, Joi.string().email());
    if (result.error !== null) {
      console.error('Email must be a valid email address');
      return;
    }

    await DBManager.sync();

    const user = await User.findOne(
      {
        where:
          { [Op.or]:
            [
              { username: argv.user },
              { email: argv.email }
            ]
          },
        attributes: { exclude: ['created_at', 'updated_at'] }
      });

    if (user && user.username) {
      Console.error('User/Email already exists!');
    } else {
      const role = argv.admin ? 'ADMIN' : 'USER';

      const password = await inputPassword();
      await User.create({
        username: argv.user,
        email: argv.email,
        date_register: new Date(),
        address_register: '127.0.0.1',
        password,
        role
      });

      Console.log(`Added new ${role.toLowerCase()}: ${argv.user}`);
    }

    await DBManager.close();
  }
};

Yargs
  .usage('admin <command> [options]')
  .command(addUserCommand)
  .demandCommand()
  .help()
  .argv;

