const Glue = require('glue');
const Config = require('config');
const DBManager = require('./utils/database').Manager;
let hapiServer = {};

const server = {};
server.manifest = {
  port: 8000,
  host: '0.0.0.0',
  routes: {
    cors: { origin: ['*'] }
  }
};

server.options = {
  relativeTo: __dirname
};

server.configure = (manifest = {}, options = {}) => {
  server.manifest = Object.assign({}, manifest);
  server.options = Object.assign({}, options);
};

server.start = async () => {
  try {
    await DBManager.sync(Config.database.options);
    hapiServer = await Glue.compose(server.manifest, server.options);
    await hapiServer.start();

    console.log('Server started at: ', hapiServer.info.uri);
  }
  catch (err) {
    console.error(err);
    process.exit(1);
  }
};

server.getInstance = () => hapiServer;

module.exports = server;
