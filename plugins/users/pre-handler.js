const Config = require('config');
const Boom = require('boom');
const Bcrypt = require('bcrypt');
const UserModel = require('./model');

const preHandler = {};

preHandler.checkUsernameAvailability = async (request, h) => {
  const payload = request.payload;
  const user = await UserModel.getAuthUser(payload.username);
  if (user && user.username) {
    throw Boom.badRequest('Username already exist');
  }
  return h.continue;
};

preHandler.checkEmailAvailability = async (request, h) => {
  const payload = request.payload;
  const user = await UserModel.getAuthEmail(payload.email);
  if (user && user.email) {
    throw Boom.badRequest('Email already exist');
  }
  return h.continue;
};

preHandler.checkEmailExistence = async (request, h) => {
  const payload = request.payload;
  const user = await UserModel.getAuthEmail(payload.email);
  if (!user) {
    throw Boom.badRequest('Email does not exist');
  }
  return h.continue;
};

preHandler.initNewUser = (request, h) => {
  request.payload.date_register = new Date();
  request.payload.address_register = request.headers['x-forwarded-for'] || request.info.remoteAddress;
  if (!request.payload.role) {
    request.payload.role = 'USER';
  }
  return h.continue;
};

module.exports = preHandler;
