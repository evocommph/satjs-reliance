const UserController = require('./controller');
const UserValidator = require('./validator');
const PreHandler = require('./pre-handler');

const internals = {};

module.exports = (server, options) => {
  // passed options when loading plugin available here
  // uncomment to see the content
  // console.log(options);
  server.dependency(['hapi-auth-basic', 'hapi-auth-bearer-token', 'hapi-acl-auth'], internals.after);

  server.auth.strategy('basic', 'basic', { validate: UserController.authBasic });
  server.auth.strategy('token', 'bearer-access-token', { validate: UserController.authToken });
  server.auth.strategy('tokenChain', 'bearer-access-token', { validate: UserController.authToken, allowChaining: true });
  server.auth.default('token');
};

internals.after = (server) => {
  server.route([
    {
      method: ['POST'],
      path: '/login',
      config: {
        handler: UserController.login,
        auth: {
          mode: 'required',
          strategies: ['basic']
        },
        plugins: {
          hapiAclAuth: {
            secure: false
          }
        }
      }
    },

    {
      method: ['POST'],
      path: '/logout',
      config: {
        handler: UserController.logout,
        auth: {
          mode: 'required',
          strategies: ['token']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['USER', 'ADMIN']
          }
        }
      }
    },

    {
      method: ['POST'],
      path: '/changePassword',
      config: {
        validate: UserValidator.changePassword,
        handler: UserController.changePassword,
        auth: {
          mode: 'required',
          strategies: ['tokenChain', 'basic']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['USER', 'ADMIN']
          }
        }
      }
    },

    {
      method: ['POST'],
      path: '/resetPassword',
      config: {
        validate: UserValidator.resetPassword,
        handler: UserController.resetPassword,
        auth: false,
        plugins: {
          hapiAclAuth: {
            secure: false
          }
        }
      }
    },

    {
      method: ['GET'],
      path: '/user',
      config: {
        handler: UserController.getId,
        auth: {
          mode: 'required',
          strategies: ['tokenChain', 'basic']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['USER', 'ADMIN']
          }
        }
      }
    },

    {
      method: ['GET'],
      path: '/users',
      config: {
        validate: UserValidator.getUsers,
        handler: UserController.getUsers,
        auth: {
          mode: 'required',
          strategies: ['tokenChain', 'basic']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['ADMIN']
          }
        }
      }
    },

    {
      method: ['GET'],
      path: '/users/id',
      config: {
        validate: UserValidator.getUsersId,
        handler: UserController.getUsersId,
        auth: {
          mode: 'required',
          strategies: ['tokenChain', 'basic']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['ADMIN']
          }
        }
      }
    },

    {
      method: ['PUT'],
      path: '/user',
      config: {
        validate: UserValidator.updateProfile,
        handler: UserController.updateProfile,
        auth: {
          mode: 'required',
          strategies: ['tokenChain', 'basic']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['USER', 'ADMIN']
          }
        }
      }
    },

    {
      method: ['GET'],
      path: '/user/logins',
      config: {
        validate: UserValidator.getUserLoginHistory,
        handler: UserController.getUserLoginHistory,
        auth: {
          mode: 'required',
          strategies: ['tokenChain', 'basic']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['USER', 'ADMIN']
          }
        }
      }
    },

    {
      method: ['POST'],
      path: '/user/refresh',
      config: {
        handler: UserController.refresh,
        auth: {
          mode: 'required',
          strategies: ['token']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['USER', 'ADMIN']
          }
        }
      }
    },

    {
      method: ['POST'],
      path: '/users',
      config: {
        pre: [
          { method: PreHandler.checkUsernameAvailability },
          { method: PreHandler.checkEmailAvailability },
          { method: PreHandler.initNewUser }
        ],
        validate: UserValidator.create,
        handler: UserController.create,
        auth: {
          mode: 'optional',
          strategies: ['tokenChain', 'basic']
        },
        plugins: {
          hapiAclAuth: {
            secure: false
          }
        }
      }
    },

    {
      method: ['GET'],
      path: '/users/activities',
      config: {
        validate: UserValidator.getUsersActivityLogs,
        handler: UserController.getUsersActivityLogs,
        auth: {
          mode: 'required',
          strategies: ['tokenChain', 'basic']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['ADMIN']
          }
        }
      }
    },

    {
      method: ['GET'],
      path: '/users/logins',
      config: {
        validate: UserValidator.getUsersLoginHistory,
        handler: UserController.getUsersLoginHistory,
        auth: {
          mode: 'required',
          strategies: ['tokenChain', 'basic']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['ADMIN']
          }
        }
      }
    },

    {
      method: ['PATCH'],
      path: '/users/status/{id}/{status}',
      config: {
        validate: UserValidator.updateUserStatus,
        handler: UserController.updateUserStatus,
        auth: {
          mode: 'required',
          strategies: ['tokenChain', 'basic']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['ADMIN']
          }
        }
      }
    }
  ]);

};



