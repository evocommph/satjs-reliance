const Config = require('config');
const Nodemailer = require('nodemailer');
const Bcrypt = require('bcrypt');
const Boom = require('boom');
const Moment = require('moment');
const PassGenerator = require('generate-password');
const { promisify } = require('util');
const UserModel = require('./model');
const { logger } = require('../../utils');

const nodeMailer = Nodemailer.createTransport(Config.mailer);
const sendMailer = promisify(nodeMailer.sendMail).bind(nodeMailer);

const userController = {};

userController.bcryptSalt = null;

userController.hashPassword = async (password) => {
  if (!userController.bcryptSalt) {
    userController.bcryptSalt = await Bcrypt.genSalt(Config.security.hashSaltRounds);
  }

  return await Bcrypt.hash(password, userController.bcryptSalt);
};

userController.notifyAdmin = async (request, username, subject, body) => {
  if (Config.server.email.admin) {
    const userIp = request.headers['x-forwarded-for'] || request.info.remoteAddress;

    const message = {
      to: Config.server.email.admin,
      // Subject of the message
      subject,
      // HTML body
      html: body ||
      `<html><head></head><body style="font-family: Arial; font-size: 12px;"><div>
      <p>User Address: ${userIp}</p>
      <p>Browser UserAgent: ${request.headers['user-agent']}</p>
      </div></body></html>`
    };
    try {
      await sendMailer(message);
    } catch (err) {
      logger.error(`UserController:notifyAdmin Encounter exception: ${err.message}`);
    }
  }
};

userController.authBasic = async (request, username, password, h) => {
  const subsUser = username.split(' as ');

  let loginUser = null;
  let adminUser = null;
  if (subsUser.length === 2) {
    adminUser = subsUser[0];
    loginUser = subsUser[1];
  } else {
    loginUser = username;
  }

  const userIp = request.headers['x-forwarded-for'] || request.info.remoteAddress;
  if (adminUser) {
    const result = await userController.validateAuth(request, adminUser, password, h);
    if (!result.credentials) {
      return result;
    }

    UserModel.logUserLogin({
      user_id: result.credentials.id,
      login: result.isValid,
      request_address: userIp
    });

    if (result.isValid && result.credentials.roles.indexOf('ADMIN') !== -1 ) {
      const user = await UserModel.getAuthUser(loginUser);
      if (!user) {
        return Boom.badRequest('Invalid user.');
      }

      result.credentials = { id: user.id, username: user.username, email: user.email, roles: [user.role] };
      return result;
    }

    throw Boom.forbidden('Requires admin privilege.');
  } else {
    const result = await userController.validateAuth(request, loginUser, password, h);
    if (!result.credentials) {
      return result;
    }

    UserModel.logUserLogin({
      user_id: result.credentials.id,
      login: result.isValid,
      request_address: userIp
    });

    return result;
  }
};

userController.validateAuth = async (request, username, password, h) => {
  const user = await UserModel.getAuthUser(username);
  if (!user) {
    return { credentials: null, isValid: false };
  }

  if (user.status === 'BLOCK') {
    throw Boom.forbidden('Your account has been blocked. Please contact administrator');
  }

  // Check if we have reach maximum login attempt
  const loginHistory = await UserModel.userLastLoginHistory(user.id
    , Moment().subtract(Config.security.maxLoginAttemptWithin, 'minutes').toDate()
    , Config.security.maxLoginAttempt);
  if (loginHistory && loginHistory.length >= Config.security.maxLoginAttempt) {

    // Is there at least one success login?
    const successLogin = loginHistory.find((logs) => logs.login);
    if (!successLogin) {
      // If we have reach maximum login attempt
      // Check the last login date
      const lastLoginDate = Moment(loginHistory[0].created_at);
      if (lastLoginDate.add(Config.security.loginLockTime, 'minutes').isAfter(Moment())) {
        userController.notifyAdmin(request, username, `Cat.io User ${username} reach maximum login attempt`);

        throw Boom.forbidden('User has reached the maximum number of login attempts.');
      }
    }
  }

  try {
    const isValid = await Bcrypt.compare(password, user.password);
    const credentials = { id: user.id, username: user.username, email: user.email, roles: [user.role] };

    return { isValid, credentials };
  } catch (err) {
    throw Boom.internal(err);
  }
};

userController.authToken = async (request, token, h) => {
  const user = await UserModel.getUserToken(token);
  if (!user) {
    return { credentials: {}, isValid: false };
  }

  const userDb = await UserModel.getAuthUser(user.username);
  if (!userDb) {
    return { credentials: {}, isValid: false };
  }

  if (userDb.status === 'BLOCK') {
    throw Boom.forbidden('Your account has been blocked. Please contact administrator');
  }

  try {
    // Refresh token on successful auth
    // No need to wait for the response
    UserModel.refreshUserToken(token);

    const credentials = { token, id: user.id, username: user.username, email: user.email, roles: user.roles };
    return { isValid: true, credentials };
  } catch (err) {
    throw Boom.internal(err);
  }
};

userController.getId = async (request, h) =>  await UserModel.getId(request.auth.credentials.id);

userController.getUsers = async (request, h) => {
  return await UserModel.getUsers(request.query.includeAdmin);
};

userController.getUsersId = async (request, h) => {
  return await UserModel.getUsersId(request.query.includeAdmin);
};

userController.login = async (request, h) => {
  userController.notifyAdmin(request, request.auth.credentials.username
    , `Cat.io User ${request.auth.credentials.username} has login`);
  const result = await UserModel.createUserToken(request.auth.credentials);

  result.isAdmin = request.auth.credentials.roles ? request.auth.credentials.roles.indexOf('ADMIN') > -1 : false;
  return result;
};

userController.logout = async (request, h) => {
  await UserModel.deleteUserToken(request.auth.credentials.token);
  const credentials = Object.assign({}, request.auth.credentials);
  delete credentials.id;
  delete credentials.token;

  userController.notifyAdmin(request, credentials.username, `Cat.io User ${credentials.username} has logout`);

  return { credentials, logoutTime: new Date().toISOString() };
};

userController.changePassword = async (request, h) => {
  const hashPassword = await userController.hashPassword(request.payload.password);

  await UserModel.changePassword(request.auth.credentials.id, hashPassword);

  // userController.notifyAdmin(request, request.auth.credentials.username
  //  , `Cat.io User ${request.auth.credentials.username} has change password`);
  
  return { userId: request.auth.credentials.id };
};

userController.resetPassword = async (request, h) => {
  const user = await UserModel.getAuthEmail(request.payload.email);
  if (user) {
    const newPassword = PassGenerator.generate({
      length: 10,
      numbers: true
    });

    const message = {
      to: request.payload.email,
      // Subject of the message
      subject: 'Cat.io Forgot Password', //
      // HTML body
      html:
      `<html><head></head><body style="font-family: Arial; font-size: 12px;"><div>
      <p>You have requested a password reset.</p>
      <p>Please use this temporary password and update your password immediately.</p>
      <p><b>${newPassword}</b></p>
      </div></body></html>`
    };

    try {
      await sendMailer(message);
      userController.notifyAdmin(request, request.payload.email, `Cat.io User ${request.payload.email} has forgot password`);
    } catch (err) {
      throw Boom.internal('Unable to send email');
    }

    const hashPassword = await userController.hashPassword(newPassword);
    await UserModel.changePassword(user.id, hashPassword);
    return { email: user.email };
  }

  throw Boom.badRequest(`Email ${request.payload.email} does not exist`);
};

userController.refresh = (request, h) => {
  return {
    token: request.auth.credentials.token,
    isAdmin: request.auth.credentials.roles ? request.auth.credentials.roles.indexOf('ADMIN') > -1 : false
  };
};

userController.create = async (request, h) => {
  request.payload.password = await userController.hashPassword(request.payload.password);
  if (request.payload.role === 'ADMIN') {
    if (request.auth && request.auth.credentials && request.auth.credentials) {
      const isAdmin = request.auth.credentials.roles.indexOf('ADMIN') > -1;
      if (!isAdmin) {
        throw Boom.forbidden('Admin user is only allowed to create admin role.');
      }
    } else {
      throw Boom.forbidden('Admin user is only allowed to create admin role.');
    }
  }

  const userId = await UserModel.create(request.payload);
  return { userId };
};

userController.updateProfile = async (request, h) => {
  const userId = await UserModel.updateProfile(request.auth.credentials.id, request.payload);
  userController.notifyAdmin(request, request.auth.credentials.username
    , `Cat.io User ${request.auth.credentials.username} updated their profile`);
  return { userId: request.auth.credentials.id };
};

userController.getUserLoginHistory = async (request, h) => {
  return await UserModel.userLastLoginHistory(request.auth.credentials.id, null
    , request.query.limit, request.query.offset);
};

userController.getUsersLoginHistory = async (request, h) => {
  return await UserModel.usersLastLoginHistory(request.query.includeAdmin, request.query.limit, request.query.offset);
};

userController.getUsersActivityLogs = async (request, h) => {
  return await UserModel.usersLastActivityLogs(request.query.includeData
    , request.query.includeAdmin, request.query.limit, request.query.offset);
};

userController.updateUserStatus = async (request, h) => {
  return await UserModel.updateUserStatus(request.params.id, request.params.status);
};

module.exports = userController;
