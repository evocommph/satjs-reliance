const Config = require('config');
const Boom = require('boom');
const Redis = require('redis');
const Moment = require('moment');
const { Op } = require('sequelize');
const { promisify } = require('util');
const { User, Profile, UserLoginHistory, UserActivityLog } = require('../../utils/database').Schema;
const { logger, uuid } = require('../../utils');

const userModel = {};

const redisClient = Redis.createClient(Config.redis);
const redisSetExAsync = promisify(redisClient.setex).bind(redisClient);
const redisGetAsync = promisify(redisClient.get).bind(redisClient);
const redisExpireAsync = promisify(redisClient.expire).bind(redisClient);
const redisDeleteAsync = promisify(redisClient.del).bind(redisClient);

userModel.getId = async (id) => {
  try {
    return await User.findById(id, {
      attributes: { exclude: ['password', 'created_at', 'updated_at'] },
      include: [{ model: Profile, attributes: { exclude: ['id', 'user_id', 'created_at', 'updated_at'] } }]
    });
  } catch (err) {
    logger.error(`UserModel:getId Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

userModel.getUser = async (username) => {
  try {
    return await User.findOne({ where: { username },
      attributes: { exclude: ['password', 'created_at', 'updated_at'] },
      include: [{ model: Profile, attributes: { exclude: ['id', 'created_at', 'updated_at'] } }]
    });
  } catch (err) {
    logger.error(`UserModel:getUser Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

userModel.getUser = async (username) => {
  try {
    return await User.findOne({ where: { username },
      attributes: { exclude: ['password', 'created_at', 'updated_at'] },
      include: [{ model: Profile, attributes: { exclude: ['id', 'created_at', 'updated_at'] } }]
    });
  } catch (err) {
    logger.error(`UserModel:getUser Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

userModel.getUsers = async (includeAdmin = false) => {
  const where = { role: 'USER' };
  if (includeAdmin) {
    delete where.role;
  }

  try {
    return await User.findAll({
      where,
      attributes: { exclude: ['password'] },
      include: [{ model: Profile, attributes: { exclude: ['id'] } }]
    });
  } catch (err) {
    logger.error(`UserModel:getUsers Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

userModel.getUsersId = async (includeAdmin = false) => {
  const where = { role: 'USER' };
  if (includeAdmin) {
    delete where.role;
  }

  try {
    return await User.findAll({
      where,
      attributes: ['id', 'username']
    });
  } catch (err) {
    logger.error(`UserModel:getUsersId Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

userModel.getEmail = async (email) => {
  try {
    return await User.findOne({ where: { email },
      attributes: { exclude: ['password', 'created_at', 'updated_at'] },
      include: [{ model: Profile, attributes: { exclude: ['id', 'user_id', 'created_at', 'updated_at'] } }]
    });
  } catch (err) {
    logger.error(`UserModel:getEmail Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }

  return {};
};

userModel.getAuthUser = async (username) => {
  try {
    return await User.findOne({ where: { username },
      attributes: { exclude: ['created_at', 'updated_at'] }
    });
  } catch (err) {
    logger.error(`UserModel:getAuthUser Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

userModel.getAuthEmail = async (email) => {
  try {
    return await User.findOne({ where: { email },
      attributes: { exclude: ['created_at', 'updated_at'] }
    });
  } catch (err) {
    logger.error(`UserModel:getAuthEmail Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

userModel.create = async (user) => {
  try {
    let newUser = null;
    if (user.profile) {
      newUser = await User.create(user, { include: Profile, as: 'profile' });
    } else {
      newUser = await User.create(user);
    }

    return newUser.id;
  } catch (err) {
    logger.error(`UserModel:create Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

userModel.changePassword = async (id, password) => {
  try {
    return await User.update({ password }, { where: { id } });
  } catch (err) {
    logger.error(`UserModel:changePassword Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

userModel.updateProfile = async (id, profile) => {
  try {
    profile.user_id = id;

    const currentProfile = await Profile.findOne({ where: { user_id: profile.user_id }, attributes: ['id'] });
    if (currentProfile) {
      // If we found the profile set it
      profile.id = currentProfile.id;
    }

    return await Profile.upsert(profile);
  } catch (err) {
    logger.error(`UserModel:updateProfile Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

userModel.logUserLogin = async (log) => {
  try {
    await UserLoginHistory.create(log);
  } catch (err) {
    logger.error(`UserModel:logUserLogin Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

userModel.userLastLoginHistory = async (id, startDate, limit = 5, offset = 0) => {
  try {
    const where = { user_id: id };
    if (startDate) {
      where.created_at = { [Op.gt]: startDate };
    }

    return await UserLoginHistory.findAll({
      order: [['created_at', 'DESC']],
      where,
      limit,
      offset
    });
  } catch (err) {
    logger.error(`UserModel:userLastLoginHistory Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

userModel.usersLastLoginHistory = async (includeAdmin = false, limit = 5, offset = 0) => {
  try {
    const userWhere = { role: 'USER' };
    if (includeAdmin) {
      delete userWhere.role;
    }

    return await UserLoginHistory.findAll({
      limit,
      offset,
      order: [['created_at', 'DESC']],
      include: [
        {
          model: User,
          where: userWhere,
          attributes: { exclude: ['password', 'created_at', 'updated_at'] }
        }
      ]
    });
  } catch (err) {
    logger.error(`UserModel:usersLastLoginHistory Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

userModel.usersLastActivityLogs = async (includeData = false, includeAdmin = false, limit = 30, offset = 0) => {
  try {
    const exclude = ['user_id', 'event'];
    if (!includeData) {
      exclude.push('data');
    }

    const userWhere = { role: 'USER' };
    if (includeAdmin) {
      delete userWhere.role;
    }

    return await UserActivityLog.findAll({
      where: { event: 'onResponse' },
      attributes: { exclude },
      order: [['created_at', 'DESC']],
      include: [
        {
          model: User,
          where: userWhere,
          attributes: { exclude: ['id', 'password', 'date_register'] }
        }],
      limit,
      offset
    });
  } catch (err) {
    logger.error(`UserModel:usersLastActivityLogs Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

userModel.createUserToken = async (user) => {
  try {
    const token = uuid();
    await redisSetExAsync(token, Config.security.userTokenTTL, JSON.stringify(user));

    return { token };
  } catch (err) {
    logger.error(`UserModel:createUserToken Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

userModel.deleteUserToken = async (token) => {
  try {
    const count = await redisDeleteAsync(token);
    return { token, deleted: count === 1 ? true : false };
  } catch (err) {
    logger.error(`UserModel:deleteUserToken Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

userModel.refreshUserToken = async (token) => {
  try {
    return await redisExpireAsync(token, Config.security.userTokenTTL);
  } catch (err) {
    logger.error(`UserModel:refreshUserToken Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

userModel.getUserToken = async (token) => {
  try {
    const user = await redisGetAsync(token);
    if (user) {
      return JSON.parse(user.toString());
    }
  } catch (err) {
    logger.error(`UserModel:getUserToken Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }

  return null;
};

userModel.updateUserStatus = async (id, status) => {
  try {
    return await User.update( { status: status.toUpperCase() }, { where: { id }, limit: 1 } );
  } catch (err) {
    logger.error(`UserModel:updateUserStatus Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }

  return null;
};

module.exports = userModel;
