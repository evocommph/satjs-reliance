const Config = require('config');
const Joi = require('joi');
const Boom = require('boom');
const userValidator = {};

userValidator.failAction = (request, h, err) => {
  throw Boom.badRequest(err.message);
};

userValidator.getId = {
  params: {
    id: Joi.number().positive()
  },

  failAction: userValidator.failAction
};

userValidator.getUser = {
  params: {
    username: Joi.string()
  },

  failAction: userValidator.failAction
};

userValidator.getUsers = {
  query: {
    limit: Joi.number().default(10),
    offset: Joi.number().default(0),
    includeAdmin: Joi.boolean().truthy([1, 'true', 't']).falsy([0,'false','f']).default(false).insensitive()
  },

  failAction: userValidator.failAction
};

userValidator.getUsersId = {
  query: {
    limit: Joi.number().default(10),
    offset: Joi.number().default(0),
    includeAdmin: Joi.boolean().truthy([1, 'true', 't']).falsy([0,'false','f']).default(false).insensitive()
  },

  failAction: userValidator.failAction
};

userValidator.getUsersId = {
  query: {
    limit: Joi.number().default(10),
    offset: Joi.number().default(0),
    includeAdmin: Joi.boolean().truthy([1, 'true', 't']).falsy([0,'false','f']).default(false).insensitive()
  },

  failAction: userValidator.failAction
};

userValidator.getEmail = {
  params: {
    email: Joi.string().email()
  },

  failAction: userValidator.failAction
};

userValidator.create = {
  payload: Joi.object({
    username: Joi.string().regex(/^[a-zA-Z0-9]+(?:[-_.]?[a-zA-Z0-9]+)*$/)
      .min(3).max(30).required(),
    password: Joi.string().min(Config.security.minPasswordLength).required(),
    email: Joi.string().email().required(),
    role: Joi.string().default('USER'),
    profile: Joi.object().keys({
      firstname: Joi.string(),
      lastname: Joi.string(),
      middlename: Joi.string(),
      phone: Joi.string(),
      mobile: Joi.string(),
      address1: Joi.string(),
      address2: Joi.string(),
      city: Joi.string(),
      state: Joi.string(),
      country: Joi.string(),
      zipcode: Joi.string()
    }).and('firstname', 'lastname')
      .and('address1', 'city', 'state', 'country', 'zipcode')
  }).unknown(false),

  failAction: userValidator.failAction
};

userValidator.changePassword = {
  payload: Joi.object({
    password: Joi.string().min(Config.security.minPasswordLength)
  }).unknown(false),

  failAction: userValidator.failAction
};

userValidator.resetPassword = {
  payload: Joi.object({
    email: Joi.string().email()
  }).unknown(false),

  failAction: userValidator.failAction
};

userValidator.updateProfile = {
  payload: Joi.object({
    firstname: Joi.string(),
    lastname: Joi.string(),
    middlename: Joi.string(),
    phone: Joi.string(),
    mobile: Joi.string().required(),
    address1: Joi.string(),
    address2: Joi.string(),
    city: Joi.string(),
    state: Joi.string(),
    country: Joi.string(),
    zipcode: Joi.string()
  }).and('firstname', 'lastname')
    .and('address1', 'city', 'state', 'country', 'zipcode')
    .unknown(false),

  failAction: userValidator.failAction
};

userValidator.getUserLoginHistory = {
  query: {
    limit: Joi.number().default(10),
    offset: Joi.number().default(0)
  },

  failAction: userValidator.failAction
};

userValidator.getUsersLoginHistory = {
  query: {
    limit: Joi.number().default(10),
    offset: Joi.number().default(0)
  },

  failAction: userValidator.failAction
};

userValidator.getUsersActivityLogs = {
  query: {
    limit: Joi.number().default(10),
    offset: Joi.number().default(0),
    includeData: Joi.boolean().truthy([1, 'true', 't']).falsy([0,'false','f']).default(false).insensitive(),
    includeAdmin: Joi.boolean().truthy([1, 'true', 't']).falsy([0,'false','f']).default(false).insensitive()
  },

  failAction: userValidator.failAction
};

userValidator.updateUserStatus = {
  params: {
    id: Joi.number().required(),
    status: Joi.string().valid('active', 'inactive', 'block').required()
  },

  failAction: userValidator.failAction
};

module.exports = userValidator;
