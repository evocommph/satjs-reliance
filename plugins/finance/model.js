const Config = require('config');
const Boom = require('boom');
const Moment = require('moment');
const Sequelize = require('sequelize');
const DBManager = require('../../utils/database').Manager;
const { User, Profile, Asset, Rate, CurrentRate, UserAssetTransaction, UserAccountPayout } = require('../../utils/database').Schema;
const { logger } = require('../../utils');
const { Op } = Sequelize;

const coinSDKModel = {};

let financeConfig = null;
if (Config.server.financeService === 'coinsdk') {
  financeConfig = Config.coinsdk;
} else if (Config.server.financeService === 'tiingo') {
  financeConfig = Config.tiingo;
}

coinSDKModel.getUser = async (userId) => {
  try {
    return await User.findById(userId, {
      attributes: { exclude: ['password', 'created_at', 'updated_at'] }
    });
  } catch (err) {
    logger.error(`UserModel:getId Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

coinSDKModel.getAssets = async (typeIsCrypto, customAsset = null, onlySupported = false) => {
  try {
    const where = {};
    if (typeIsCrypto >= 0) {
      where.type_is_crypto = typeIsCrypto;
    }

    if (customAsset >= 0) {
      where.custom_asset = customAsset;
    }

    if (onlySupported > 0) {
      if (where.type_is_crypto > 0 && Array.isArray(financeConfig.filters) && financeConfig.filters.length > 0) {
        where.asset_id = { [Op.in]: financeConfig.filters };
      } else if (where.type_is_crypto === 0 && Array.isArray(financeConfig.quotes) && financeConfig.quotes.length > 0) {
        where.asset_id = { [Op.in]: financeConfig.quotes };
      }
    }

    if (Config.server.financeService === 'tiingo') {
      if (where.type_is_crypto === 0) {
        return [{ asset_id: 'USD', name: 'US Dollar' }];
      }
      delete where.type_is_crypto;
    }

    return await Asset.findAll({
      where,
      attributes: { exclude: ['id', 'created_at', 'updated_at'] },
      include: [
        {
          model: CurrentRate,
          where: { asset_id_quote: 'USD' },
          required: false,
          attributes: ['asset_id_quote', 'rate', 'previous_rate']
        }
      ]
    });
  } catch (err) {
    logger.error(`CoinSDKModel:getAssets Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

coinSDKModel.getAssetsId = async (typeIsCrypto, onlySupported = false) => {
  try {
    const where = {};
    if (typeIsCrypto >= 0) {
      where.type_is_crypto = typeIsCrypto;
    }

    if (onlySupported > 0) {
      if (where.type_is_crypto > 0 && Array.isArray(financeConfig.filters) && financeConfig.filters.length > 0) {
        where.asset_id = { [Op.in]: financeConfig.filters };
      } else if (where.type_is_crypto === 0 && Array.isArray(financeConfig.quotes) && financeConfig.quotes.length > 0) {
        where.asset_id = { [Op.in]: financeConfig.quotes };
      }
    }

    if (Config.server.financeService === 'tiingo') {
      if (where.type_is_crypto === 0) {
        return [ 'USD' ];
      }
      delete where.type_is_crypto;
    }

    return await Asset.findAll({
      where,
      attributes: ['asset_id', 'name']
    }).then((assets) => assets.map((asset) => asset.asset_id));
  } catch (err) {
    logger.error(`CoinSDKModel:getAssets Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

coinSDKModel.getAsset = async (id) => {
  try {
    return await Asset.findById(id, {
      attributes: { exclude: ['created_at', 'updated_at'] }
    });
  } catch (err) {
    logger.error(`CoinSDKModel:getAsset Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

coinSDKModel.deleteAsset = async (id) => {
  try {
    const ratesId = await Rate.findAll({ where: { asset_id: id }, attributes: ['id'] });
    await UserAssetTransaction.destroy({
      where: { rate_id: { [Op.in] : ratesId.map((rate) => rate.id) } }
    });
    await Rate.destroy({ where: { asset_id: id } });
    await CurrentRate.destroy({ where: { asset_id: id } });
    return await Asset.destroy({ where: { asset_id: id }, limit: 1 });
  } catch (err) {
    logger.error(`CoinSDKModel:deleteAsset Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

coinSDKModel.createAsset = async (asset) => {
  if (await coinSDKModel.getAsset(asset.asset_id)) {
    throw Boom.badRequest('Asset id must be unique.');
  }

  // All user created asset mark it as custom
  asset.custom_asset = true;
  try {
    return await Asset.create(asset);
  } catch (err) {
    logger.error(`CoinSDKModel:createAsset Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

coinSDKModel.updateAsset = async (asset) => {
  try {
    return await Asset.update(asset, { where: { asset_id: asset.asset_id } } );
  } catch (err) {
    logger.error(`CoinSDKModel:updateAsset Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

coinSDKModel.upsertAsset = async (asset) => {
  try {
    return await Asset.upsert(asset);
  } catch (err) {
    logger.error(`CoinSDKModel:getAsset Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

coinSDKModel.getRateById = async (id) => {
  try {
    return await Rate.findById(id);
  } catch (err) {
    logger.error(`CoinSDKModel:getRateById Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

coinSDKModel.getCurrentRates = async (quotes = [], customAsset = null) => {
  try {
    let where = undefined;
    if (Array.isArray(quotes) && quotes.length > 0) {
      where = { asset_id_quote: { [Op.in] : quotes } };
    } else if (typeof quotes === 'string') {
      where = { asset_id_quote: quotes  };
    }

    if (customAsset !== null) {
      where['$Asset.custom_asset$'] = customAsset;
    }

    return await CurrentRate.findAll({
      where,
      attributes: {
        exclude: ['created_at', 'updated_at']
      },
      include: [
        {
          model: Asset,
          as: 'Asset',
          attributes: ['name']
        }
      ]
    });
  } catch (err) {
    logger.error(`CoinSDKModel:getCurrentRates Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

coinSDKModel.getCurrentRate = async (assetId, quotes = [], customAsset = null) => {
  try {
    const where = { asset_id: assetId };
    if (Array.isArray(quotes) && quotes.length > 0) {
      where.asset_id_quote = { [Op.in] : quotes };
    } else if (typeof quotes === 'string') {
      where.asset_id_quote = quotes;
    }

    if (customAsset !== null) {
      where['$Asset.custom_asset$'] = customAsset;
    }

    return await CurrentRate.findOne({
      where,
      attributes: {
        exclude: ['created_at', 'updated_at']
      },
      include: [
        {
          model: Asset,
          as: 'Asset',
          attributes: ['name']
        }
      ]
    });
  } catch (err) {
    logger.error(`CoinSDKModel:getCurrentRate Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

coinSDKModel.getRates = async (quotes = []) => {
  try {
    let where = undefined;
    if (quotes && quotes.length > 0) {
      where = { asset_id_quote: { [Op.in] : quotes } };
    }

    return await Rate.findAll({
      where,
      attributes: {
        exclude: ['created_at', 'updated_at']
      }
    });
  } catch (err) {
    logger.error(`CoinSDKModel:getRates Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

coinSDKModel.getRate = async (assetId, quotes = [], startTime = null, endTime = null) => {
  try {
    const where = { asset_id: assetId };
    if (quotes && quotes.length > 0) {
      where.asset_id_quote = { [Op.in] : quotes };
    }

    if (startTime) {
      where.time = { [Op.gte] : new Date(startTime) };
    }

    if (endTime) {
      where.time = { [Op.lte] : new Date(endTime) };
    }

    return await Rate.findAll({
      where,
      attributes: {
        exclude: ['id', 'created_at', 'updated_at']
      },
      order: [['time', 'ASC']]
    });
  } catch (err) {
    logger.error(`CoinSDKModel:getRate Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

coinSDKModel.getSpecificRate = async (rate) => {
  try {
    return await Rate.findOne({
      where: { asset_id: rate.asset_id, asset_id_quote: rate.asset_id_quote, time: rate.time },
      attributes: { exclude: ['created_at', 'updated_at'] }
    });
  } catch (err) {
    logger.error(`CoinSDKModel:getSpecificRate Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

coinSDKModel.createRate = async (rate) => {
  try {
    return await Rate.create(rate);
  } catch (err) {
    logger.error(`CoinSDKModel:createRate Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

coinSDKModel.createCurrentRate = async (rate) => {
  try {
    if (!rate.previous_rate) {
      rate.previous_rate = rate.rate;
      rate.previous_time = rate.time;
    }

    return await CurrentRate.create(rate);
  } catch (err) {
    logger.error(`CoinSDKModel:createRate Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

coinSDKModel.updateCurrentRate = async (rate) => {
  try {
    return await CurrentRate.update({
      previous_rate: Sequelize.col('rate'),
      previous_time: Sequelize.col('time'),
      rate: rate.rate,
      time: rate.time
    }, {
      where: {
        asset_id: rate.asset_id,
        asset_id_quote: rate.asset_id_quote
      }
    });
  } catch (err) {
    logger.error(`CoinSDKModel:createRate Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

coinSDKModel.upsertRate = async (rate) => {
  try {
    return await coinSDKModel.getSpecificRate(rate).then((dbRate) => {
      if (!(dbRate && dbRate.asset_id)) {
        return coinSDKModel.createRate(rate);
      }
    }).then(() => {
      return coinSDKModel.getCurrentRate(rate.asset_id, rate.asset_id_quote);
    }).then((dbRate) => {
      if (dbRate && dbRate.asset_id) {
        return coinSDKModel.updateCurrentRate(rate);
      }
      return coinSDKModel.createCurrentRate(rate);
    }).catch((err) => {
      throw Boom.internal(err.message);
    });
  } catch (err) {
    logger.error(`CoinSDKModel:upsertRate Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

coinSDKModel.createUserAssetTransaction = async (transaction) => {
  try {
    return await UserAssetTransaction.create(transaction);
  } catch (err) {
    logger.error(`CoinSDKModel:createUserAssetTransaction Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

coinSDKModel.getUserTotalAssets = async (userId, assetId) => {
  try {
    const buyAsset = await UserAssetTransaction.findOne({
      where: { user_id: userId, action: 'BUY', status: 'APPROVED' },
      attributes: {
        include: ['id', [Sequelize.fn('sum', Sequelize.col('quantity')), 'quantity']]
      },
      include: [
        {
          model: Rate,
          where: { asset_id: assetId },
          attributes: { exclude: ['id', 'created_at', 'updated_at'] }
        }
      ],
      group: ['asset_id']
    });

    const sellAsset = await UserAssetTransaction.findOne({
      where: { user_id: userId, action: 'SELL', status: { [Op.in]: ['APPROVED', 'PENDING'] } },
      attributes: {
        include: ['id', [Sequelize.fn('sum', Sequelize.col('quantity')), 'quantity']]
      },
      include: [
        {
          model: Rate,
          where: { asset_id: assetId },
          attributes: { exclude: ['id', 'created_at', 'updated_at'] }
        }
      ],
      group: ['asset_id']
    });

    const buyQuantity = buyAsset ? buyAsset.quantity : 0;
    const sellQuantity = sellAsset ? sellAsset.quantity : 0;

    console.log(`buyQuantity: ${buyQuantity} vs sellQuantity: ${sellQuantity}`);

    return (buyQuantity - sellQuantity);
  } catch (err) {
    logger.error(`CoinSDKModel:getUserTotalAssets Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

coinSDKModel.getUserAssetTransactions = async (userId, action, status) => {
  try {
    const where = { user_id:userId };
    if (action) {
      where.action = action.toUpperCase();
    }

    if (status) {
      where.status = status.toUpperCase();
    }

    return await UserAssetTransaction.findAll({
      where,
      attributes: { exclude: ['rate_id', 'user_id', 'updated_at'] },
      include: [
        {
          model: Rate,
          attributes: { exclude: ['id', 'created_at', 'updated_at'] },
          include: [
            {
              model: Asset, as: 'Asset',
              attributes: ['name']
            }
          ]
        }
      ]
    });
  } catch (err) {
    logger.error(`CoinSDKModel:getUserAssetTransactions Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

coinSDKModel.getUsersAssetTransactions = async (action, status) => {
  try {
    const where = {};
    if (action) {
      where.action = action.toUpperCase();
    }

    if (status) {
      where.status = status.toUpperCase();
    }

    return await UserAssetTransaction.findAll({
      where,
      attributes: { exclude: ['rate_id', 'user_id', 'updated_at'] },
      include: [
        {
          model: User,
          attributes: ['username', 'email'],
          include: [
            {
              model: Profile,
              attributes: { exclude: ['id', 'created_at', 'updated_at'] }
            }
          ]
        },
        {
          model: Rate,
          attributes: { exclude: ['id', 'created_at', 'updated_at'] },
          include: [
            {
              model: Asset, as: 'Asset',
              attributes: ['name']
            }
          ]
        }
      ]
    });
  } catch (err) {
    logger.error(`CoinSDKModel:getUserAssetTransactions Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

coinSDKModel.getUserAssetTransaction = async (userId, assetId, action, status) => {
  try {
    const where = { user_id:userId };
    if (action) {
      where.action = action.toUpperCase();
    }

    if (status) {
      where.status = status.toUpperCase();
    }

    return await UserAssetTransaction.findAll({
      where,
      attributes: { exclude: ['rate_id', 'user_id', 'updated_at'] },
      include: [
        {
          model: Rate,
          where: { asset_id: assetId },
          attributes: { exclude: ['id', 'created_at', 'updated_at'] }
        }
      ]
    });
  } catch (err) {
    logger.error(`CoinSDKModel:getUserAssetTransaction Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

coinSDKModel.getUserAssetTransactionId = async (userId, transactionId) => {
  try {
    const where = { user_id:userId, id: transactionId };

    return await UserAssetTransaction.findOne({
      where,
      attributes: { exclude: ['rate_id', 'user_id', 'updated_at'] },
      include: [
        {
          model: Rate,
          attributes: { exclude: ['id', 'created_at', 'updated_at'] }
        }
      ]
    });
  } catch (err) {
    logger.error(`CoinSDKModel:getUserAssetTransactionId Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

coinSDKModel.updateUserAssetTransactionStatus = async (transactionId, status) => {
  try {
    const where = { id: transactionId };

    if (status.toUpperCase() === 'APPROVED') {
      const transaction = await UserAssetTransaction.findById(transactionId, {
        include: [
          {
            model: Rate,
            attributes: { exclude: ['id', 'created_at', 'updated_at'] }
          }
        ] });
      if (transaction.action === 'BUY') {
        const buyPrice = transaction.quantity * transaction.rate.rate;

        // Compute total credit payouts.
        const payouts = await UserAccountPayout.findOne({
          where: { user_id: transaction.user_id },
          attributes: [
            [Sequelize.fn('sum', Sequelize.col('payout')), 'totalPayout']
          ]
        });

        const sellAsset = await UserAssetTransaction.findOne({
          where: { user_id: transaction.user_id, action: 'SELL', status: 'APPROVED' },
          attributes: [
            [Sequelize.fn('sum', Sequelize.col('sell_price')), 'totalAmount']
          ],
          include: [
            {
              model: Rate,
              where: { id: Sequelize.col('rate_id') },
              attributes: []
            }
          ]
        });

        const totalPayout = (+payouts.get('totalPayout') || 0);
        const totalAmount = (+sellAsset.get('totalAmount') || 0);
        const availablePayout = totalAmount - totalPayout;
        if ( availablePayout > 0 ) {
          const deductPayout = (availablePayout > buyPrice ? buyPrice : availablePayout).toFixed(2);
          await UserAccountPayout.create( {
            user_id: transaction.user_id,
            payout: deductPayout,
            credit: true
          });
        }
      }
    }

    return await UserAssetTransaction.update({
      status: status.toUpperCase(),
      status_change_date: Sequelize.fn('now') }, { where, limit: 1 });
  } catch (err) {
    logger.error(`CoinSDKModel:deleteUserAssetTransactionId Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

coinSDKModel.deleteUserAssetTransactionId = async (userId, transactionId) => {
  try {
    const where = { user_id:userId, id: transactionId };

    return await UserAssetTransaction.destroy({ where, limit: 1 });
  } catch (err) {
    logger.error(`CoinSDKModel:deleteUserAssetTransactionId Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

coinSDKModel.getUserTransactionAssetsId = async (userId) => {
  try {
    return await Asset.findAll({
      attributes: ['asset_id', 'name'],
      group: ['asset_id'],
      include: [
        {
          model: Rate,
          attributes: [],
          required: true,
          include: [
            {
              model: UserAssetTransaction,
              where: { user_id: userId },
              attributes: [],
              required: true
            }
          ]
        }
      ]
    });
  } catch (err) {
    logger.error(`CoinSDKModel:getUserAssetTransaction Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

coinSDKModel.countUserAssetsTransactionStatus = async (id, status) => {
  try {
    const count = await UserAssetTransaction.count({ where: { user_id: id, status: status.toUpperCase(), status_change_unread: 1 } });
    return { count };
  } catch (err) {
    logger.error(`CoinSDKModel:countUserAssetsTransactionStatus Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

coinSDKModel.countUsersAssetsTransactionStatus = async (status) => {
  try {
    const count = await UserAssetTransaction.count({ where: { status: status.toUpperCase() } });
    return { count };
  } catch (err) {
    logger.error(`CoinSDKModel:countUsersAssetsTransactionStatus Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

coinSDKModel.readUserAssetsTransactionStatus = async (id, status) => {
  try {
    const count = await UserAssetTransaction.update({
      status_change_unread: 0
    }, { where: { user_id: id, status: status.toUpperCase(), status_change_unread: 1 } });
    return { count };
  } catch (err) {
    logger.error(`CoinSDKModel:readUserAssetsTransactionStatus Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

coinSDKModel.removeRejectedTransactions = async () => {
  try {
    const expiryDate = Moment().subtract(Config.user.userAssetTransaction.timers.removeRejected.after,'d').toDate();
    await UserAssetTransaction.destroy(
      {
        where: {
          status: 'REJECTED',
          status_change_unread: 0,
          updated_at: {
            [Op.lt]: expiryDate
          }
        }
      });
  } catch (err) {
    logger.warning(`CoinSDKModel:removeRejectedTransactions Encounter exception: ${err.message}`);
  }
};

coinSDKModel.getUsersVirtualMoney = async (id) => {
  try {
    const buyAsset = await UserAssetTransaction.findAll({
      where: { user_id: id, action: 'BUY', status: 'APPROVED' },
      attributes: [[Sequelize.fn('sum', Sequelize.col('quantity')), 'buyQuantity']],
      include: [
        {
          model: Rate,
          where: { id: Sequelize.col('rate_id') },
          attributes: ['asset_id']
        }
      ],
      group: ['asset_id']
    });

    const sellAsset = await UserAssetTransaction.findAll({
      where: { user_id: id, action: 'SELL', status: 'APPROVED' },
      attributes: [
        [Sequelize.fn('sum', Sequelize.col('quantity')), 'sellQuantity'],
        [Sequelize.fn('sum', Sequelize.col('sell_price')), 'totalSellPrice']
      ],
      include: [
        {
          model: Rate,
          where: { id: Sequelize.col('rate_id') },
          attributes: ['asset_id']
        }
      ],
      group: ['rate.asset_id']
    });

    const currentAssets = buyAsset.map((buyTransaction) => {
      return buyTransaction.rate.asset_id;
    });

    const currentRates = await CurrentRate.findAll({
      where: { asset_id: buyAsset.map((bt) => bt.rate.asset_id), asset_id_quote: 'USD' },
      attributes: ['asset_id', 'rate']
    });

    const buyAndSellAsset = buyAsset.map((buyTransaction) => {
      const combineTransaction = {
        assetId: buyTransaction.rate.asset_id,
        buyQuantity: +buyTransaction.get('buyQuantity'),
        sellQuantity: 0,
        totalSellPrice: 0.0
      };

      const transaction = sellAsset.find((sellTransaction) => {
        return buyTransaction.rate.asset_id === sellTransaction.rate.asset_id;
      });

      if (transaction) {
        combineTransaction.sellQuantity = +transaction.get('sellQuantity');
        combineTransaction.totalSellPrice = +transaction.get('totalSellPrice');
      } else {
        combineTransaction.sellQuantity = 0;
        combineTransaction.totalSellPrice = 0;
      }

      const currentRate = currentRates.find((rate) => {
        return rate.asset_id === buyTransaction.rate.asset_id;
      });

      combineTransaction.rate = +currentRate.rate;

      return combineTransaction;
    });

    let money = 0.0;
    buyAndSellAsset.forEach((asset) => {
      const totalBuy = (asset.buyQuantity - asset.sellQuantity) * asset.rate;
      const totalSell = asset.totalSellPrice;
      money += totalBuy + totalSell;
    });

    // Compute total payouts and deduct to total money.
    const payouts = await UserAccountPayout.findOne({
      where: { user_id: id },
      attributes: [
        [Sequelize.fn('sum', Sequelize.col('payout')), 'totalPayout']
      ]
    });

    money -= +(payouts.get('totalPayout') || 0);
    return { money };
  } catch (err) {
    logger.error(`CoinSDKModel:getUsersVirtualMoney Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

coinSDKModel.getUsersVirtualSell = async (id) => {
  try {
    const sellAsset = await UserAssetTransaction.findOne({
      where: { user_id: id, action: 'SELL', status: 'APPROVED' },
      attributes: [
        [Sequelize.fn('sum', Sequelize.col('sell_price')), 'totalAmount']
      ],
      include: [
        {
          model: Rate,
          where: { id: Sequelize.col('rate_id') },
          attributes: []
        }
      ]
    });

    const payouts = await UserAccountPayout.findOne({
      where: { user_id: id },
      attributes: [
        [Sequelize.fn('sum', Sequelize.col('payout')), 'totalPayout']
      ]
    });

    const sellAmount = +sellAsset.get('totalAmount');
    const payoutAmount = +payouts.get('totalPayout');
    const availableAmount = (sellAmount || 0).toFixed(2) - (payoutAmount || 0).toFixed(2);

    return { money: availableAmount };
  } catch (err) {
    logger.error(`CoinSDKModel:getUsersVirtualSell Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

coinSDKModel.getUserAccountHoldings = async (id) => {
  try {
    const buyAsset = await UserAssetTransaction.findAll({
      where: { user_id: id, action: 'BUY', status: 'APPROVED' },
      attributes: [
        [Sequelize.fn('sum', Sequelize.col('quantity')), 'buyQuantity'],
        [Sequelize.literal('sum(rate.rate * quantity)'), 'sumRate']
      ],
      include: [
        {
          model: Rate,
          where: { id: Sequelize.col('rate_id') },
          attributes: ['asset_id'],
          include: [
            {
              model: Asset, as: 'Asset',
              attributes: ['name']
            }
          ]
        }
      ],
      group: ['rate.asset_id']
    });

    const sellAsset = await UserAssetTransaction.findAll({
      where: { user_id: id, action: 'SELL', status: 'APPROVED' },
      attributes: [
        [Sequelize.fn('sum', Sequelize.col('quantity')), 'sellQuantity'],
        [Sequelize.fn('sum', Sequelize.col('sell_price')), 'totalSellPrice']
      ],
      include: [
        {
          model: Rate,
          where: { id: Sequelize.col('rate_id') },
          attributes: ['asset_id']
        }
      ],
      group: ['rate.asset_id']
    });

    const currentRates = await CurrentRate.findAll({
      where: { asset_id: buyAsset.map((bt) => bt.rate.asset_id), asset_id_quote: 'USD' },
      attributes: ['asset_id', 'rate']
    });

    const buyAndSellAsset = buyAsset.map((buyTransaction) => {
      const combineTransaction = {
        assetId: buyTransaction.rate.asset_id,
        name: buyTransaction.rate.Asset.name,
        buyQuantity: +buyTransaction.get('buyQuantity'),
        buyValue: (+buyTransaction.get('sumRate') / +buyTransaction.get('buyQuantity')),
        sellQuantity: 0,
        sellPrice: 0
      };

      const transaction = sellAsset.find((sellTransaction) => {
        return buyTransaction.rate.asset_id === sellTransaction.rate.asset_id;
      });

      if (transaction) {
        combineTransaction.sellQuantity = +transaction.get('sellQuantity');
        combineTransaction.sellPrice = +transaction.get('totalSellPrice');
      } else {
        combineTransaction.sellQuantity = 0;
        combineTransaction.sellPrice = 0;
      }

      const currentRate = currentRates.find((rate) => {
        return rate.asset_id === buyTransaction.rate.asset_id;
      });

      combineTransaction.rate = +currentRate.rate;

      return combineTransaction;
    });

    const accountHoldings = buyAndSellAsset.map((asset) => {
      return {
        assetId: asset.assetId,
        name: asset.name,
        quantity: (asset.buyQuantity - asset.sellQuantity),
        currentPrice: (asset.buyQuantity - asset.sellQuantity) * asset.rate,
        buyValue: asset.buyValue,
        currentValue: asset.rate
      };
    }).filter((asset) => {
      return asset.quantity > 0.0;
    });

    return accountHoldings;
  } catch (err) {
    logger.error(`CoinSDKModel:getUserAccountHoldings Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

coinSDKModel.getUserAccountPayouts = async (id) => {
  try {
    return await UserAccountPayout.findAll({
      where: { user_id: id }
    });
  } catch (err) {
    logger.error(`CoinSDKModel:getUserAccountPayouts Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

coinSDKModel.createUserAccountPayout = async (payout) => {
  const sellAsset = await UserAssetTransaction.findOne({
    where: { user_id: payout.user_id, action: 'SELL', status: 'APPROVED' },
    attributes: [
      [Sequelize.fn('sum', Sequelize.col('sell_price')), 'totalAmount']
    ],
    include: [
      {
        model: Rate,
        where: { id: Sequelize.col('rate_id') },
        attributes: []
      }
    ]
  });

  const payouts = await UserAccountPayout.findOne({
    where: { user_id: payout.user_id },
    attributes: [
      [Sequelize.fn('sum', Sequelize.col('payout')), 'totalPayout']
    ]
  });

  const sellAmount = +sellAsset.get('totalAmount');
  const payoutAmount = +payouts.get('totalPayout');
  const availableAmount = ((sellAmount || 0).toFixed(2) - (payoutAmount || 0).toFixed(2)) - payout.payout.toFixed(2);
  if (availableAmount < 0 ) {
    throw Boom.badRequest('User don\'t have enough amount/payout.');
  }

  try {
    payout.credit = true; // mark it as credit to user.
    return await UserAccountPayout.create(payout);
  } catch (err) {
    logger.error(`CoinSDKModel:createUserAccountPayout Encounter exception: ${err.message}`);
    throw Boom.badImplementation(err);
  }
};

module.exports = coinSDKModel;
