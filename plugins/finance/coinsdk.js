const Config = require('config');
const Http = require('http');
const Https = require('https');
const Wreck = require('wreck');

class MetaData {
  constructor(wreck) {
    this.wreck = wreck;
  }

  listAllExchanges() {
    return this.wreck.get('/v1/exchanges');
  }

  listAllAssets() {
    return this.wreck.get('/v1/assets');
  }

  listAllSymbols() {
    return this.wreck.get('/v1/symbols');
  }

  transformAssets(response) {
    return JSON.parse(response.payload.toString());
  }
}

class ExchangeRates {
  constructor(wreck) {
    this.wreck = wreck;
  }

  getSpecificRates(assetIdBase, assetIdQuote, time) {
    let uri = `/v1/exchangerate/${assetIdBase}/${assetIdQuote}`;
    if (time) {
      uri = `${uri}?time=${time.toISOString()}`;
    }

    return this.wreck.get(uri);
  }

  getAllCurrentRates(assetIdBase) {
    throw new Exception()
  }

  transformRate(response) {
    return JSON.parse(response.payload.toString());
  }
}

class OHLVC {
  constructor(wreck) {
    this.wreck = wreck;
  }

  listAllPeriods() {
    return this.wreck.get('/v1/ohlcv/periods');
  }

  listLatestData(symbolId, periodId, limit) {
    let uri = `/v1/ohlcv/${symbolId}/latest?period_id=${periodId}`;
    if (limit) {
      uri = `${uri}&limit=${limit}`;
    }

    return this.wreck.get(uri);
  }

  historicalData(symbolId, periodId, timeStart, timeEnd, limit) {
    let uri = `/v1/ohlcv/${symbolId}/history?period_id=${periodId}&time_start=${timeStart.toISOString()}`;
    if (timeEnd) {
      uri = `${uri}&time_end=${timeEnd.toISOString()}`;
    }

    if (limit) {
      uri = `${uri}&limit=${limit}`;
    }

    return this.wreck.get(uri);
  }
}

class Trades {
  constructor(wreck) {
    this.wreck = wreck;
  }

  latestDataAll(limit) {
    let uri = '/v1/trades/latest';
    if (limit) {
      uri = `${uri}?limit=${limit}`;
    }

    return this.wreck.get(uri);
  }

  latestDataSymbol(symbolId, limit) {
    let uri = `/v1/trades/${symbolId}/latest`;
    if (limit) {
      uri = `${uri}?limit=${limit}`;
    }

    return this.wreck.get(uri);
  }

  latestHistoricalData(symbolId, timeStart, timeEnd, limit) {
    let uri = `/v1/trades/${symbolId}/history?time_start=${timeStart.toISOString()}`;
    if (timeEnd) {
      uri = `${uri}&time_end=${timeEnd.toISOString()}`;
    }

    if (limit) {
      uri = `${uri}&limit=${limit}`;
    }

    return this.wreck.get(uri);
  }
}

class Quotes {
  constructor(wreck) {
    this.wreck = wreck;
  }

  currentDataAll() {
    this.wreck.get('/v1/quotes/current');
  }

  currentDataSymbol(symbolId) {
    this.wreck.get(`/v1/quotes/${symbolId}/current`);
  }

  latestDataAll(limit) {
    let uri = '/v1/quotes/latest';
    if (limit) {
      uri = `${uri}?limit=${limit}`;
    }

    this.wreck.get(uri);
  }

  latestDataSymbol(symbolId, limit) {
    let uri = `/v1/quotes/${symbolId}/current`;
    if (limit) {
      uri = `${uri}?limit=${limit}`;
    }

    return this.wreck.get(uri);
  }

  historicalData(symbolId, timeStart, timeEnd, limit) {
    let uri = `/v1/quotes/${symbolId}/history?time_start=${timeStart.toISOString()}`;
    if (timeEnd) {
      uri = `${uri}&time_end=${timeEnd.toISOString()}`;
    }

    if (limit) {
      uri = `${uri}&limit=${limit}`;
    }

    return this.wreck.get(uri);
  }
}

class OrderBooks {
  constructor(wreck) {
    this.wreck = wreck;
  }

  currentDataAll() {
    return this.wreck.get('/v1/orderbooks/current');
  }

  currentDataSymbol(symbolId) {
    return this.wreck.get(`/v1/orderbooks/${symbolId}/current`);
  }

  latestData(symbolId, limit) {
    let uri = `/v1/orderbooks/${symbolId}/latest`;
    if (limit) {
      uri = `${uri}?limit=${limit}`;
    }

    this.wreck.get(uri);
  }

  historicalData(symbolId, timeStart, timeEnd, limit) {
    let uri = `/v1/orderbooks/${symbolId}/history?time_start=${timeStart.toISOString()}`;
    if (timeEnd) {
      uri = `${uri}&time_end=${timeEnd.toISOString()}`;
    }

    if (limit) {
      uri = `${uri}&limit=${limit}`;
    }

    return this.wreck.get(uri);
  }

}

class CoinSDK {
  constructor(apiKey) {

    this.wreck = Wreck.defaults({
      baseUrl: Config.coinsdk.url.stage,
      agents: {
        https: new Https.Agent({ maxSockets: 100 }),
        http: new Http.Agent({ maxSockets: 1000 }),
        httpsAllowUnauthorized: new Https.Agent({ maxSockets: 100, rejectUnauthorized: false })
      }
      //timeout: 30000 // 30 seconds timeout
    });

    apiKey = apiKey || Config.coinsdk.key;
    if (apiKey) {
      this.wreck = this.wreck.defaults({
        baseUrl: Config.coinsdk.url.live,
        headers: {
          'X-CoinAPI-Key': apiKey
        }
      });
    }

    this.metadata = new MetaData(this.wreck);
    this.exchangeRates = new ExchangeRates(this.wreck);
    this.ohlvc = new OHLVC(this.wreck);
    this.trades = new Trades(this.wreck);
    this.quotes = new Quotes(this.wreck);
    this.orderBooks = new OrderBooks(this.wreck);
  }
}

module.exports = CoinSDK;
