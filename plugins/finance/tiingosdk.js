const Config = require('config');
const Http = require('http');
const Https = require('https');
const Wreck = require('wreck');
const Moment = require('moment');
const Boom = require('boom');
const { logger } = require('../../utils');

class MetaData {
  constructor(wreck) {
    this.wreck = wreck;
  }

  async listAllAssets() {
    const assets = [];
    for (const asset of Config.tiingo.assets ) {
      try {
        const response = await this.wreck.get(`/tiingo/daily/${asset}`);
        const metadata = JSON.parse(response.payload.toString());
        assets.push(metadata);
      } catch (err) {
        // Do nothing for now
        logger.error(`CoinIO:MetaData:listAllAssets: ${asset} throws an error ${err.message}`);
      }
    }
    return { payload: assets };
  }

  transformAssets(response) {
    const assets = [];
    response.payload.forEach((metadata) => {
      assets.push({
        asset_id: metadata.ticker,
        name: metadata.name || metadata.ticker,
        exchange_code: metadata.exchangeCode,
        data_start: metadata.startDate,
        data_end: metadata.endDate,
        type_is_crypto: 0,
        finance_type: 'stocks'
      });
    });

    return assets;
  }
}

class ExchangeRates {
  constructor(wreck) {
    this.wreck = wreck;
  }

  getSpecificRates(assetIdBase, assetIdQuote, time) {
    let uri = `/tiingo/daily/${assetIdBase}/prices`;
    if (time) {
      uri = `${uri}?startDate=${Moment(time).format('YYYY-MM-DD')}`;
    }

    return this.wreck.get(uri);
  }

  getAllCurrentRates(assetIdBase) {
    throw Boom.notImplemented('Tiingo only support USD');
  }

  transformRate(response) {
    let rate = JSON.parse(response.payload.toString());
    if (Array.isArray(rate) && rate.length > 0) {
      rate = rate[0];
      return {
        asset_id_quote: 'USD',
        time: rate.date,
        rate: rate.adjClose || rate.close
      };
    }

    return {};
  }
}

class TiingoSDK {
  constructor(apiKey) {

    this.wreck = Wreck.defaults({
      baseUrl: Config.tiingo.url.stage,
      agents: {
        https: new Https.Agent({ maxSockets: 100 }),
        http: new Http.Agent({ maxSockets: 1000 }),
        httpsAllowUnauthorized: new Https.Agent({ maxSockets: 100, rejectUnauthorized: false })
      }
      //timeout: 30000 // 30 seconds timeout
    });

    apiKey = apiKey || Config.tiingo.key;
    if (apiKey) {
      this.wreck = this.wreck.defaults({
        baseUrl: Config.tiingo.url.live,
        headers: {
          Authorization: `token ${apiKey}`
        }
      });
    }

    this.metadata = new MetaData(this.wreck);
    this.exchangeRates = new ExchangeRates(this.wreck);
  }
}

module.exports = TiingoSDK;
