const Config = require('config');
const BaseJoi = require('joi');
const Extension = require('joi-date-extensions');
const Boom = require('boom');

const Joi = BaseJoi.extend(Extension);

const coinSdkValidator = {};

coinSdkValidator.failAction = (request, h, err) => {
  if (err.details && err.details.length > 0) {
    throw Boom.badRequest(err.details[0].message);
  }
  throw Boom.badRequest(err.message);
};

coinSdkValidator.getAssets = {
  query: {
    limit: Joi.number().default(10),
    offset: Joi.number().default(0),
    typeIsCrypto: Joi.number().integer().min(0).max(1),
    onlySupported: Joi.number().integer().min(0).max(1).default(0),
    customAssets: Joi.number().integer().min(0).max(1)
  },

  failAction: coinSdkValidator.failAction
};

coinSdkValidator.getAssetsId = {
  query: {
    limit: Joi.number().default(10),
    offset: Joi.number().default(0),
    typeIsCrypto: Joi.number().integer().min(0).max(1),
    onlySupported: Joi.number().integer().min(0).max(1).default(0)
  },

  failAction: coinSdkValidator.failAction
};

coinSdkValidator.getAsset = {
  query: {
    limit: Joi.number().default(10),
    offset: Joi.number().default(0)
  },

  params: {
    asset: Joi.string()
  },

  failAction: coinSdkValidator.failAction
};

coinSdkValidator.deleteAsset = {
  params: {
    asset: Joi.string()
  },

  failAction: coinSdkValidator.failAction
};

coinSdkValidator.createAsset = {
  payload: Joi.object({
    asset_id: Joi.string().required(),
    name: Joi.string().required(),
    type_is_crypto: Joi.number().integer().min(0).max(1).required(),
    data_start: Joi.date().max(Joi.ref('data_end')).format('YYYY-MM-DD'),
    data_end: Joi.date().format('YYYY-MM-DD'),
    data_quote_start: Joi.date().max(Joi.ref('data_quote_end')).iso(),
    data_quote_end: Joi.date().iso(),
    data_orderbook_start: Joi.date().max(Joi.ref('data_orderbook_end')).iso(),
    data_orderbook_end: Joi.date().iso(),
    data_trade_start: Joi.date().max(Joi.ref('data_trade_end')).iso(),
    data_trade_end: Joi.date().iso(),
    data_trade_count: Joi.number().integer(),
    data_symbols_count: Joi.number().integer()
  }).and('data_start', 'data_end')
    .and('data_quote_start', 'data_quote_end')
    .and('data_orderbook_start', 'data_orderbook_end')
    .unknown(false),

  failAction: coinSdkValidator.failAction
};

coinSdkValidator.createRate = {
  payload: Joi.object({
    asset_id: Joi.string().required(),
    asset_id_quote: Joi.string().required(),
    time: Joi.date().iso(),
    rate: Joi.number().required()
  }).unknown(false),

  failAction: coinSdkValidator.failAction
};

coinSdkValidator.getRate = {
  query: {
    limit: Joi.number().default(10),
    offset: Joi.number().default(0),
    startTime: Joi.number(),
    endTime: Joi.number(),
    quote: Joi.alternatives().try(Joi.string(), Joi.array().items(Joi.string()).min(1))
  },

  params: {
    asset: Joi.string()
  },

  failAction: coinSdkValidator.failAction
};

coinSdkValidator.getRates = {
  query: {
    limit: Joi.number().default(10),
    offset: Joi.number().default(0),
    quote: Joi.alternatives().try(Joi.string(), Joi.array().items(Joi.string()).min(1))
  },

  failAction: coinSdkValidator.failAction
};

coinSdkValidator.doTransaction = {
  params: {
    action: Joi.string().valid('buy', 'sell')
  },

  payload: Joi.object({
    user_id: Joi.number(),
    asset_id: Joi.string().required(),
    asset_id_quote: Joi.string().required(),
    quantity: Joi.number().integer().required(),
    sell_price: Joi.number()
  }).unknown(false),

  failAction: coinSdkValidator.failAction
};

coinSdkValidator.getTransaction = {
  query: {
    limit: Joi.number().default(10),
    offset: Joi.number().default(0),
    status: Joi.string().valid('rejected', 'approved', 'pending')
  },

  params: {
    asset: Joi.string(),
    action: Joi.string().valid('buy', 'sell')
  },

  failAction: coinSdkValidator.failAction
};

coinSdkValidator.getTransactions = {
  query: {
    limit: Joi.number().default(10),
    offset: Joi.number().default(0),
    status: Joi.string().valid('rejected', 'approved', 'pending')
  },

  params: {
    action: Joi.string().valid('buy', 'sell')
  },

  failAction: coinSdkValidator.failAction
};

coinSdkValidator.updateTransactionStatus = {
  params: {
    id: Joi.number().required(),
    status: Joi.string().valid('rejected', 'approved', 'pending').required()
  },

  failAction: coinSdkValidator.failAction
};

coinSdkValidator.getAllTransactions = {
  query: {
    limit: Joi.number().default(10),
    offset: Joi.number().default(0),
    status: Joi.string().valid('rejected', 'approved', 'pending')
  },

  params: {
    action: Joi.string().valid('buy', 'sell')
  },

  failAction: coinSdkValidator.failAction
};

coinSdkValidator.countUserAssetsTransactionStatus = {
  params: {
    status: Joi.string().valid('rejected', 'approved', 'pending')
  },

  failAction: coinSdkValidator.failAction
};

coinSdkValidator.countUsersAssetsTransactionStatus = {
  params: {
    status: Joi.string().valid('rejected', 'approved', 'pending')
  },

  failAction: coinSdkValidator.failAction
};

coinSdkValidator.readUserAssetsTransactionStatus = {
  params: {
    status: Joi.string().valid('rejected', 'approved', 'pending')
  },

  failAction: coinSdkValidator.failAction
};

coinSdkValidator.createUserAccountPayout = {
  payload: Joi.object({
    user_id: Joi.number().required(),
    payout: Joi.number().greater(0).precision(13).required()
  }).unknown(false),

  failAction: coinSdkValidator.failAction
};

coinSdkValidator.getUsersVirtualSell = {
  params: {
    id: Joi.number()
  },

  failAction: coinSdkValidator.failAction
};


module.exports = coinSdkValidator;
