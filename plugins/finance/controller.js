const Config = require('config');
const Moment = require('moment');
const Boom = require('boom');
const CoinSDK = require('./coinsdk');
const TiingoSDK = require('./tiingosdk');
const CoinModel = require('./model');
const UserController = require('../users/controller');
const { logger } = require('../../utils');

let financeSDK = null;
let financeConfig = null;
if (Config.server.financeService === 'coinsdk') {
  financeSDK = new CoinSDK();
  financeConfig = Config.coinsdk;
} else if (Config.server.financeService === 'tiingo') {
  financeSDK = new TiingoSDK();
  financeConfig = Config.tiingo;
}

const controller = {};

controller.assetTimer = null;
controller.ratesTimer = null;

controller.notifyAdmin = async (request, username, transaction, rate, subject) => {
  const userIp = request.headers['x-forwarded-for'] || request.info.remoteAddress;

  const html =
  `<html><head></head><body style="font-family: Arial; font-size: 12px;"><div>
    <p>User Address: ${userIp}</p>
    <p>Browser UserAgent: ${request.headers['user-agent']}</p>
    <p>Action: ${transaction.action}</p>
    <p>Target Asset: ${rate.asset_id}</p>
    <p>Quote Asset: ${rate.asset_id_quote}</p>
    <p>Quantity: ${transaction.quantity}</p>
    <p>Amount: ${transaction.quantity * rate.rate}</p>
    <p>Current Rate Amount: ${rate.rate}</p>
    <p>Current Rate Time: ${rate.time}</p>
    </div></body></html>`;

  await UserController.notifyAdmin(request, username, subject, html);
};

controller.startTimers = async () => {
  //await controller.refreshAssets();
  //await controller.refreshRates();
  await controller.removeRejectedTransactions();

  //controller.restartAssetTimer();
  //controller.restartRatesTimer();
  controller.removeRejectedTransactionsTimer();
};

controller.restartAssetTimer = () => {
  if (controller.assetTimer) {
    // reset interval if needed
    clearInterval(controller.assetTimer);
  }

  controller.assetTimer = setInterval(() => {
    controller.refreshAssets();
  }, Moment.duration(financeConfig.timers.asset, 'minutes').asMilliseconds());
};

controller.restartRatesTimer = () => {
  if (controller.ratesTimer) {
    // reset interval if needed
    clearInterval(controller.ratesTimer);
  }

  controller.ratesTimer = setInterval(() => {
    controller.refreshRates();
  }, Moment.duration(financeConfig.timers.exchangeRates, 'minutes').asMilliseconds());
};

controller.removeRejectedTransactionsTimer = () => {
  if (controller.rejectedTimer) {
    // reset interval if needed
    clearInterval(controller.rejectedTimer);
  }

  controller.rejectedTimer = setInterval(() => {
    controller.removeRejectedTransactions();
  }, Moment.duration(Config.user.userAssetTransaction.timers.removeRejected.poll, 'minutes').asMilliseconds());
};

controller.createAsset = async (request) => {
  return await CoinModel.createAsset(request.payload);
};

controller.getAssets = async (request) => {
  let typeIsCrypto = null;
  let customAsset = null;
  let onlySupported = null;
  if (request.query) {
    typeIsCrypto = +request.query.typeIsCrypto;
    customAsset = +request.query.customAsset;
    onlySupported = request.query.onlySupported === 1;
  }

  return await CoinModel.getAssets(typeIsCrypto, customAsset, onlySupported);
};

controller.getAssetsId = async (request) => {
  let typeIsCrypto = null;
  let onlySupported = null;
  if (request.query) {
    typeIsCrypto = request.query.typeIsCrypto;
    onlySupported = request.query.onlySupported === 1;
  }

  return await CoinModel.getAssetsId(typeIsCrypto, onlySupported);
};

controller.getAsset = async (request) => {
  const asset = typeof request === 'string' ? request : request.params.asset;
  return await CoinModel.getAsset(asset);
};

controller.deleteAsset = async (request) => {
  const asset = typeof request === 'string' ? request : request.params.asset;
  return await CoinModel.deleteAsset(asset);
};

controller.refreshAssets = async () => {
  const response = await financeSDK.metadata.listAllAssets();
  const assets = financeSDK.metadata.transformAssets(response);

  for (const asset of assets) {
    await CoinModel.upsertAsset(asset);
  }

  // Reset asset timer
  controller.restartAssetTimer();

  // Return the list of assets
  return assets;
};

controller.refreshRates = async () => {
  let assets = await CoinModel.getAssetsId(1, true);
  if (Array.isArray(financeConfig.filters) && financeConfig.filters.length > 0) {
    assets = assets.filter((asset) => {
      return financeConfig.filters.findIndex((filterAsset) => {
        return filterAsset === asset;
      }) >= 0;
    });
  }

  try {
    for (const asset of assets) {
      await controller.refreshRate(asset);
    }
  } catch (err) {
    // Do nothing for now
    logger.error(`CoinIO:Controller:refreshRates: refreshRates throws an error ${err.message}`);
  }

  // Reset asset timer
  controller.restartRatesTimer();

  // Return the list of assets
  return assets;
};

controller.removeRejectedTransactions = async () => {
  await CoinModel.removeRejectedTransactions();

  // Reset asset timer
  controller.removeRejectedTransactionsTimer();
};

controller.createRate = async (request) => {
  return await CoinModel.upsertRate(request.payload);
};

controller.getCurrentRates = async (request) => {
  let quotes = null;
  let customAsset = null;
  if (request.query) {
    if (typeof request.query.quote === 'string') {
      quotes = [request.query.quote];
    } else if (Array.isArray(request.query.quote)) {
      quotes = request.query.quote;
    }

    if (request.query.custom_asset) {
      customAsset = +request.query.custom_asset;
    }
  }

  return await CoinModel.getCurrentRates(quotes, customAsset);
};

controller.getCurrentRate = async (request) => {
  let quotes = null;
  let customAsset = null;
  if (request.query) {
    if (typeof request.query.quote === 'string') {
      quotes = [request.query.quote];
    } else if (Array.isArray(request.query.quote)) {
      quotes = request.query.quote;
    }

    if (request.query.custom_asset) {
      customAsset = +request.query.custom_asset;
    }
  }

  return await CoinModel.getCurrentRate(request.params.asset, quotes, customAsset);
};

controller.getRates = async (request) => {
  let quotes = null;
  if (request.query) {
    if (typeof request.query.quote === 'string') {
      quotes = [request.query.quote];
    } else if (Array.isArray(request.query.quote)) {
      quotes = request.query.quote;
    }

  }

  return await CoinModel.getRates(quotes);
};

controller.getRate = async (request) => {
  let quotes = null;
  if (request.query) {
    if (typeof request.query.quote === 'string') {
      quotes = [request.query.quote];
    } else if (Array.isArray(request.query.quote)) {
      quotes = request.query.quote;
    }
  }

  return await CoinModel.getRate(request.params.asset, quotes, request.query.startTime, request.query.endTime);
};

controller.refreshRate = async (request) => {
  const assetId = typeof request === 'string' ? request : request.payload.asset;
  if (financeConfig.quotes && financeConfig.quotes.length > 0) {
    for (const quote of financeConfig.quotes) {
      const response = await financeSDK.exchangeRates.getSpecificRates(assetId, quote);
      const rate = financeSDK.exchangeRates.transformRate(response);
      rate.asset_id = assetId;
      await CoinModel.upsertRate(rate);
    }
  } else {
    const response = await financeSDK.exchangeRates.getAllCurrentRates(assetId);
    const exchangeRates = JSON.parse(response.payload.toString());
    for (const rate of exchangeRates.rates) {
      rate.asset_id = assetId;
      await CoinModel.upsertRate(rate);
    }
  }
};

controller.doTransaction = async (request) => {
  const action = request.params.action.toUpperCase();

  let userId = request.auth.credentials.id;
  if (request.payload.user_id) {
    const isAdmin = request.auth.credentials.roles.indexOf('ADMIN') > -1;
    if (!isAdmin) {
      throw Boom.forbidden('Admin user is only allowed to buy assets for a user.');
    }
    userId = request.payload.user_id;
  }

  const currentRate = await CoinModel.getCurrentRate(request.payload.asset_id, [request.payload.asset_id_quote]);
  const rate = await CoinModel.getSpecificRate(currentRate);
  if (rate) {
    if (action === 'BUY') {
      return await controller.doBuyTransaction(request, userId, rate);
    } else if (action === 'SELL') {
      return await controller.doSellTransaction(request, userId, rate);
    }
    throw Boom.badRequest('Invalid action');
  }

  throw Boom.badRequest('Invalid asset id and/or quote');
};

controller.doBuyTransaction = async (request, userId, currentRate) => {
  const transaction = request.payload;
  transaction.user_id = userId;
  transaction.action = 'BUY';
  transaction.rate_id = currentRate.id;
  transaction.status = 'PENDING';

  const user = await CoinModel.getUser(userId);
  if (!user) {
    throw Boom.badRequest('Invalid user id');
  }

  const result = await CoinModel.createUserAssetTransaction(transaction);

  controller.notifyAdmin(request, user.username, transaction, currentRate
    , `Cat.io User ${user.username} buy ${currentRate.asset_id}`);
  return result;
};

controller.doSellTransaction = async (request, userId, currentRate) => {
  const transaction = request.payload;
  transaction.user_id = userId;
  transaction.action = 'SELL';
  transaction.rate_id = currentRate.id;
  transaction.status = 'PENDING';

  const user = await CoinModel.getUser(userId);
  if (!user) {
    throw Boom.badRequest('Invalid user id');
  }

  const totalAsset = await CoinModel.getUserTotalAssets(userId, currentRate.asset_id);

  console.log(`totalAsset: ${totalAsset} vs transaction.quantity: ${transaction.quantity}`);

  if (totalAsset < 0) {
    controller.notifyAdmin(request, user.username, transaction, currentRate
      , `Cat.io User ${user.username} has negative (${totalAsset}) assets for ${currentRate.asset_id}`);
    throw Boom.badRequest('You don\'t have enough asset.');
  } else if (totalAsset < transaction.quantity) {
    controller.notifyAdmin(request, user.username, transaction, currentRate
      , `Cat.io User ${user.username} attempting to sell ${currentRate.asset_id} having total asset: ${totalAsset}`);
    throw Boom.badRequest('You don\'t have enough asset');
  }

  const result = await CoinModel.createUserAssetTransaction(transaction);
  controller.notifyAdmin(request, user.username, transaction, currentRate
    , `Cat.io User ${user.username} sell ${currentRate.asset_id}`);
  return result;
};

controller.addCurrentRatesInTransactions = async (transactions) => {
  const rates = {};
  const updatedTransactions = [];
  for (let transaction of transactions) {
    transaction = transaction.toJSON();

    const id = `${transaction.rate.asset_id}_${transaction.rate.asset_id_quote}`;
    if (Object.prototype.hasOwnProperty.call(rates, id)) {
      transaction.current_rate = rates[id];
    } else {
      const newRate = await CoinModel.getCurrentRate(transaction.rate.asset_id, [transaction.rate.asset_id_quote]);
      if (newRate) {
        // Get the current rate
        transaction.current_rate = newRate.toJSON();
        rates[id] = newRate;
      }
    }

    updatedTransactions.push(transaction);
  }

  return updatedTransactions;
};

controller.getTransactions = async (request) => {
  let status = null;
  if (request.query) {
    status = request.query.status;
  }

  let transactions = await CoinModel.getUserAssetTransactions(request.auth.credentials.id, request.params.action, status);
  transactions = await controller.addCurrentRatesInTransactions(transactions);
  return transactions;
};

controller.getAllTransactions = async (request) => {
  let status = null;
  if (request.query) {
    status = request.query.status;
  }

  let transactions = await CoinModel.getUsersAssetTransactions(request.params.action, status);
  transactions = await controller.addCurrentRatesInTransactions(transactions);
  return transactions;
};

controller.getTransaction = async (request) => {
  let status = null;
  if (request.query) {
    status = request.query.status;
  }

  let transactions = await CoinModel.getUserAssetTransaction(request.auth.credentials.id, request.params.asset, request.params.action, status);
  transactions = await controller.addCurrentRatesInTransactions(transactions);
  return transactions;
};

controller.removeTransaction = async (request) => {
  const result =  await CoinModel.deleteUserAssetTransactionId(request.auth.credentials.id, request.params.id);
  if (result > 0) {
    return { success: true };
  }

  throw Boom.badRequest('Invalid transaction id');
};

controller.updateTransactionStatus = async (request) => {
  await CoinModel.updateUserAssetTransactionStatus(request.params.id, request.params.status);
  return { success: true };
};

controller.getUserTransactionAssetsId = async (request) => {
  return await CoinModel.getUserTransactionAssetsId(request.auth.credentials.id);
};

controller.countUserAssetsTransactionStatus = async (request) => {
  return await CoinModel.countUserAssetsTransactionStatus(request.auth.credentials.id, request.params.status);
};

controller.countUsersAssetsTransactionStatus = async (request) => {
  return await CoinModel.countUsersAssetsTransactionStatus(request.params.status);
};

controller.readUserAssetsTransactionStatus = async (request) => {
  return await CoinModel.readUserAssetsTransactionStatus(request.auth.credentials.id, request.params.status);
};

controller.getUsersVirtualMoney = async (request) => {
  return await CoinModel.getUsersVirtualMoney(request.auth.credentials.id);
};

controller.getUsersVirtualSell = async (request) => {
  let userId = request.auth.credentials.id;
  if (request.params.id) {
    const isAdmin = request.auth.credentials.roles.indexOf('ADMIN') > -1;
    if (!isAdmin) {
      throw Boom.forbidden('Admin user is only allowed to buy assets for a user.');
    }
    userId = request.params.id;
  }

  return await CoinModel.getUsersVirtualSell(userId);
};

controller.getUsersTransactionAssetsId = async (request, id) => {
  return await CoinModel.getUserTransactionAssetsId(request.params.id);
};

controller.getUserAccountHoldings = async (request) => {
  return await CoinModel.getUserAccountHoldings(request.auth.credentials.id);
};

controller.getUserAccountPayouts = async (request) => {
  return await CoinModel.getUserAccountPayouts(request.auth.credentials.id);
};

controller.createUserAccountPayout = async (request) => {
  return await CoinModel.createUserAccountPayout(request.payload);
};

module.exports = controller;
