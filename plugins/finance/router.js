const CoinSDKController = require('./controller');
const CoinSDKValidator = require('./validator');
const PreHandler = require('./pre-handler');

const internals = {};

module.exports = (server, options) => {
  // Load users before coinsdk since users handles
  // The authentications.
  CoinSDKController.startTimers();

  server.dependency(['users'], internals.after);
};

internals.after = (server) => {
  server.route([

    {
      method: ['GET'],
      path: '/assets',
      config: {
        handler: CoinSDKController.getAssets,
        auth: {
          mode: 'required',
          strategies: ['tokenChain', 'basic']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['USER', 'ADMIN']
          }
        }
      }
    },

    {
      method: ['GET'],
      path: '/assets/id',
      config: {
        validate: CoinSDKValidator.getAssetsId,
        handler: CoinSDKController.getAssetsId,
        auth: {
          mode: 'required',
          strategies: ['tokenChain', 'basic']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['USER', 'ADMIN']
          }
        }
      }
    },

    {
      method: ['POST'],
      path: '/assets',
      config: {
        validate: CoinSDKValidator.createAsset,
        handler: CoinSDKController.createAsset,
        auth: {
          mode: 'required',
          strategies: ['tokenChain', 'basic']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['ADMIN']
          }
        }
      }
    },

    {
      method: ['GET'],
      path: '/asset/{asset}',
      config: {
        validate: CoinSDKValidator.getAsset,
        handler: CoinSDKController.getAsset,
        auth: {
          mode: 'required',
          strategies: ['tokenChain', 'basic']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['USER', 'ADMIN']
          }
        }
      }
    },

    {
      method: ['DELETE'],
      path: '/asset/{asset}',
      config: {
        validate: CoinSDKValidator.deleteAsset,
        handler: CoinSDKController.deleteAsset,
        auth: {
          mode: 'required',
          strategies: ['tokenChain', 'basic']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['USER', 'ADMIN']
          }
        }
      }
    },

    {
      method: ['POST'],
      path: '/assets/refresh',
      config: {
        handler: CoinSDKController.refreshAssets,
        auth: {
          mode: 'required',
          strategies: ['tokenChain', 'basic']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['ADMIN']
          }
        }
      }
    },

    {
      method: ['POST'],
      path: '/rates',
      config: {
        pre: [
          { method: PreHandler.initRateTime }
        ],
        validate: CoinSDKValidator.createRate,
        handler: CoinSDKController.createRate,
        auth: {
          mode: 'required',
          strategies: ['tokenChain', 'basic']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['ADMIN']
          }
        }
      }
    },

    {
      method: ['GET'],
      path: '/rates',
      config: {
        validate: CoinSDKValidator.getRates,
        handler: CoinSDKController.getRates,
        auth: {
          mode: 'required',
          strategies: ['tokenChain', 'basic']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['USER', 'ADMIN']
          }
        }
      }
    },

    {
      method: ['GET'],
      path: '/rates/current',
      config: {
        handler: CoinSDKController.getCurrentRates,
        auth: {
          mode: 'required',
          strategies: ['tokenChain', 'basic']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['USER', 'ADMIN']
          }
        }
      }
    },

    {
      method: ['GET'],
      path: '/rate/{asset}',
      config: {
        validate: CoinSDKValidator.getRate,
        handler: CoinSDKController.getRate,
        auth: {
          mode: 'required',
          strategies: ['tokenChain', 'basic']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['USER', 'ADMIN']
          }
        }
      }
    },

    {
      method: ['GET'],
      path: '/rate/{asset}/current',
      config: {
        validate: CoinSDKValidator.getRate,
        handler: CoinSDKController.getCurrentRate,
        auth: {
          mode: 'required',
          strategies: ['tokenChain', 'basic']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['USER', 'ADMIN']
          }
        }
      }
    },

    {
      method: ['POST'],
      path: '/rates/refresh',
      config: {
        handler: CoinSDKController.refreshRates,
        auth: {
          mode: 'required',
          strategies: ['tokenChain', 'basic']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['ADMIN']
          }
        }
      }
    },

    {
      method: ['POST'],
      path: '/asset/{action}',
      config: {
        validate: CoinSDKValidator.doTransaction,
        handler: CoinSDKController.doTransaction,
        auth: {
          mode: 'required',
          strategies: ['tokenChain', 'basic']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['USER', 'ADMIN']
          }
        }
      }
    },

    {
      method: ['GET'],
      path: '/assets/transactions/{action?}',
      config: {
        validate: CoinSDKValidator.getTransactions,
        handler: CoinSDKController.getTransactions,
        auth: {
          mode: 'required',
          strategies: ['tokenChain', 'basic']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['USER', 'ADMIN']
          }
        }
      }
    },

    {
      method: ['GET'],
      path: '/assets/transactions/{status}/count',
      config: {
        validate: CoinSDKValidator.countUserAssetsTransactionStatus,
        handler: CoinSDKController.countUserAssetsTransactionStatus,
        auth: {
          mode: 'required',
          strategies: ['tokenChain', 'basic']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['USER', 'ADMIN']
          }
        }
      }
    },

    {
      method: ['PATCH'],
      path: '/assets/transactions/{status}/read',
      config: {
        validate: CoinSDKValidator.readUserAssetsTransactionStatus,
        handler: CoinSDKController.readUserAssetsTransactionStatus,
        auth: {
          mode: 'required',
          strategies: ['tokenChain', 'basic']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['USER', 'ADMIN']
          }
        }
      }
    },

    {
      method: ['GET'],
      path: '/users/assets/transactions/{action?}',
      config: {
        validate: CoinSDKValidator.getAllTransactions,
        handler: CoinSDKController.getAllTransactions,
        auth: {
          mode: 'required',
          strategies: ['tokenChain', 'basic']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['ADMIN']
          }
        }
      }
    },

    {
      method: ['GET'],
      path: '/users/assets/transactions/{status}/count',
      config: {
        validate: CoinSDKValidator.countUsersAssetsTransactionStatus,
        handler: CoinSDKController.countUsersAssetsTransactionStatus,
        auth: {
          mode: 'required',
          strategies: ['tokenChain', 'basic']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['ADMIN']
          }
        }
      }
    },

    {
      method: ['GET'],
      path: '/assets/transaction/{asset}/{action?}',
      config: {
        validate: CoinSDKValidator.getTransaction,
        handler: CoinSDKController.getTransaction,
        auth: {
          mode: 'required',
          strategies: ['tokenChain', 'basic']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['USER', 'ADMIN']
          }
        }
      }
    },

    {
      method: ['PATCH'],
      path: '/users/assets/transaction/{id}/{status}',
      config: {
        validate: CoinSDKValidator.updateTransactionStatus,
        handler: CoinSDKController.updateTransactionStatus,
        auth: {
          mode: 'required',
          strategies: ['tokenChain', 'basic']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['ADMIN']
          }
        }
      }
    },

    {
      method: ['DELETE'],
      path: '/assets/transaction/{id}',
      config: {
        handler: CoinSDKController.removeTransaction,
        auth: {
          mode: 'required',
          strategies: ['tokenChain', 'basic']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['USER', 'ADMIN']
          }
        }
      }
    },

    {
      method: ['GET'],
      path: '/assets/transactions/id',
      config: {
        handler: CoinSDKController.getUserTransactionAssetsId,
        auth: {
          mode: 'required',
          strategies: ['tokenChain', 'basic']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['USER', 'ADMIN']
          }
        }
      }
    },

    {
      method: ['GET'],
      path: '/users/assets/transactions/{id}/id',
      config: {
        handler: CoinSDKController.getUsersTransactionAssetsId,
        auth: {
          mode: 'required',
          strategies: ['tokenChain', 'basic']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['ADMIN']
          }
        }
      }
    },

    {
      method: ['GET'],
      path: '/user/money',
      config: {
        handler: CoinSDKController.getUsersVirtualMoney,
        auth: {
          mode: 'required',
          strategies: ['tokenChain', 'basic']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['USER', 'ADMIN']
          }
        }
      }
    },

    {
      method: ['GET'],
      path: '/user/money/sell/{id?}',
      config: {
        validate: CoinSDKValidator.getUsersVirtualSell,
        handler: CoinSDKController.getUsersVirtualSell,
        auth: {
          mode: 'required',
          strategies: ['tokenChain', 'basic']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['USER', 'ADMIN']
          }
        }
      }
    },

    {
      method: ['GET'],
      path: '/user/account/holdings',
      config: {
        handler: CoinSDKController.getUserAccountHoldings,
        auth: {
          mode: 'required',
          strategies: ['tokenChain', 'basic']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['USER', 'ADMIN']
          }
        }
      }
    },

    {
      method: ['GET'],
      path: '/user/account/payouts',
      config: {
        handler: CoinSDKController.getUserAccountPayouts,
        auth: {
          mode: 'required',
          strategies: ['tokenChain', 'basic']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['USER', 'ADMIN']
          }
        }
      }
    },

    {
      method: ['POST'],
      path: '/user/account/payout',
      config: {
        validate: CoinSDKValidator.createUserAccountPayout,
        handler: CoinSDKController.createUserAccountPayout,
        auth: {
          mode: 'required',
          strategies: ['tokenChain', 'basic']
        },
        plugins: {
          hapiAclAuth: {
            roles: ['ADMIN']
          }
        }
      }
    }

  ]);
};

