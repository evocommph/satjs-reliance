const Moment = require('moment');

const preHandler = {};

preHandler.initRateTime = (request, h) => {
  const payload = request.payload;
  if (!payload.time) {
    // Init time
    payload.time = new Moment.utc().toISOString();
  } else {
    // Convert Time to UTC ISO String
    const time = new Moment.utc(payload.time);
    if (time.isValid()) {
      payload.time = time.toISOString();
    }
    // Let the validator handle the payload time format error
  }

  return h.continue;
};

module.exports = preHandler;
