const Config = require('config');
const Util = require('util');
const { logger } = require('../../utils');
const { UserActivityLog } = require('../../utils/database').Schema;

const plugin = {
  name: 'logger',
  version: '1.0'
};

const Nodemailer = require('nodemailer');
const { promisify } = require('util');

const nodeMailer = Nodemailer.createTransport(Config.mailer);
const sendMailer = promisify(nodeMailer.sendMail).bind(nodeMailer);

const logDb = (request, event, logData) => {
  UserActivityLog.create({
    user_id: request.auth.credentials.id,
    action: `${request.method.toUpperCase()} ${request.path}`,
    event,
    request_address: logData.ip,
    data: JSON.stringify(logData)
  });

  if (event === 'onResponse' && Config.server.email.admin) {
    let payload = request.payload;
    // Mask password for logging
    if (payload && payload.password) {
      payload.password = '*****';
    }
    payload = Util.inspect(payload, { showHidden: true, depth: null });

    let response = request.response.source;
    if (response && response.token) {
      response.token = '*****';
    }
    response = Util.inspect(response);

    const message = {
      to: Config.server.email.admin,
      // Subject of the message
      subject: `Cat.io Log Activity ${request.auth.credentials.username}`, //
      // HTML body
      html:
      `<html><head></head><body style="font-family: Arial; font-size: 12px;"><div>
      <p>Rest URL: ${request.method.toUpperCase()} ${Config.server.fqdn}${logData.reqPath}</p>
      <p>User Address: ${logData.ip}</p>
      <p>Browser UserAgent: ${logData.userAgent}</p>
      <p>Request Payload: ${payload}</p>
      <p>Response Payload: ${response}</p>
      </div></body></html>`
    };

    try {
      sendMailer(message);
    } catch (err) {
      logger.info(`Logger:logDb: SendMailer encounter an exception ${err.message}`);
    }
  }
};

const isExcludeLogActivity = (request) => {
  if (Array.isArray(Config.user.logActivity.exclude) && Config.user.logActivity.exclude.length > 0) {
    const index = Config.user.logActivity.exclude.findIndex((exclude) =>  {
      return (request.method.toUpperCase() === exclude.method.toUpperCase())
            && (request.route.path === exclude.path);
    });
    return index >= 0;
  }

  return false;
};

plugin.register = (server, options) => {

  const onPreHandler = (request, h) => {
    const payload = Util.inspect(request.payload, { showHidden: true, depth: null });
    // Mask password for logging
    if (payload && payload.password) {
      payload.password = '*****';
    }

    const logData = {
      event: 'onRequest',
      ip: request.headers['x-forwarded-for'] || request.info.remoteAddress,
      userAgent: request.headers['user-agent'],
      reqPath: `${request.method.toUpperCase()} ${request.path}`,
      reqData: {
        query: request.query,
        params: request.params,
        payload
      }
    };

    logger.info(`Plugin:onPreHandler: ${JSON.stringify(logData)}`);
    if (request.auth.credentials && request.auth.credentials.id && Config.user.logActivity.onRequest
      && !isExcludeLogActivity(request)) {
      logDb(request, 'onRequest', logData);
    }
    return h.continue;
  };

  const onResponse = (request) => {
    try {
      const responseData = Util.inspect(request.response.source);
      const logData = {
        event: 'onResponse',
        reqId: request.info.id,
        ip: request.headers['x-forwarded-for'] || request.info.remoteAddress,
        userAgent: request.headers['user-agent'],
        reqPath: `${request.method.toUpperCase()} ${request.path}`,
        resData: responseData
      };
      logger.info(`Plugin:OnResponse: ${JSON.stringify(logData)}`);
      if (request.auth.credentials && request.auth.credentials.id && Config.user.logActivity.onResponse
        && !isExcludeLogActivity(request)) {
        logDb(request, 'onResponse', logData);
      }
    } catch (err) {
      logger.error(`Plugin:onResponse: Catch exception:  ${err.message}`, err);
    }
  };

  server.ext('onPreHandler', onPreHandler);
  server.events.on('response', onResponse);

  // add more server events for log here

  server.events.on({ name: 'request', channels: 'error' }, (request, event, tags) => {
    logger.error(`Plugin:onResponse: Request ${event.request} failed`);
  });
};

module.exports = plugin;
