const internals = {};

const pluginOptions = {
  handler: () => {}
};

module.exports = (server, options) => {
  //options can be used to pass property when loading this plugin
  //look on root folder index.js

  //server.dependency(['plugin to load before this plugin'], internals.after);
  if (options.acmeResponder) {
    pluginOptions.handler = options.acmeResponder;
  }

  server.dependency([], internals.after);
};

internals.after = (server) => {
  server.route([
    {
      method: 'GET',
      path: '/.well-known/acme-challenge',
      handler: function (request, reply) {
        const req = request.raw.req;
        const res = request.raw.res;

        reply.close(false);
        pluginOptions.handler(req, res);
      }
    }
  ]);
};

