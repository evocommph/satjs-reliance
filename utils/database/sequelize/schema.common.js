const Sequelize = require('sequelize');
const DBManager = require('./manager');

const User = DBManager.define('users', {
  id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true, field: 'id' },
  username: { type: Sequelize.STRING(50), allowNull: false, unique: 'user_username', field: 'username' },
  email: { type: Sequelize.STRING(125), allowNull: false, unique: 'user_email', field: 'email' },
  password: { type: Sequelize.STRING, allowNull: false, field: 'password' },
  role: { type: Sequelize.STRING(25), default: 'USER', field: 'role' },
  address_register: { type: Sequelize.STRING(40), allowNull: true, field: 'address_register' },
  date_register: { type: Sequelize.DATE, allowNull: true, field: 'date_register' },
  status: { type: Sequelize.STRING(25), defaultValue: 'ACTIVE', field: 'status' }
}, { indexes: [
  {
    name: 'user_username',
    fields: ['username']
  },
  {
    name: 'user_email',
    fields: ['email']
  },
  {
    name: 'user_status',
    fields: ['status']
  }
], underscored: true });

const Profile = DBManager.define('profiles', {
  id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true, field: 'id' },
  lastname: { type: Sequelize.STRING(100), allowNull: false, field: 'lastname' },
  firstname: { type: Sequelize.STRING(100), allowNull: false, field: 'firstname' },
  middlename: { type: Sequelize.STRING(100), allowNull: true, field: 'middlename' },
  phone: { type: Sequelize.STRING(25), allowNull: true, field: 'phone' },
  mobile: { type: Sequelize.STRING(25), allowNull: true, field: 'mobile' },
  address1: { type: Sequelize.STRING, allowNull: true, field: 'address1' },
  address2: { type: Sequelize.STRING, allowNull: true, field: 'address2' },
  city: { type: Sequelize.STRING(50), allowNull: true, field: 'city' },
  state: { type: Sequelize.STRING(50), allowNull: true, field: 'state' },
  country: { type: Sequelize.STRING(50), allowNull: true, field: 'country' },
  zipcode: { type: Sequelize.INTEGER, allowNull: true, field: 'zipcode' }
}, { indexes: [
  {
    name: 'profiles_lastname_and_firstname',
    fields: ['lastname', 'firstname']
  },
  {
    name: 'profiles_mobile',
    fields: ['mobile']
  }
], underscored: true });

const UserActivityLog = DBManager.define('user_activity_logs', {
  id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true, field: 'id' },
  action: { type: Sequelize.STRING(50), allowNull: false, field: 'action' },
  event: { type: Sequelize.STRING(50), allowNull: false, field: 'event' },
  request_address: { type: Sequelize.STRING(40), allowNull: false, field: 'request_address' },
  data: { type: Sequelize.TEXT, allowNull: true, field: 'data' }
}, { underscored: true });

const UserLoginHistory = DBManager.define('user_login_histories', {
  id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true, field: 'id' },
  login: { type: Sequelize.BOOLEAN, default: false, field: 'login' },
  request_address: { type: Sequelize.STRING(40), allowNull: false, field: 'request_address' }
}, { underscored: true });

// TODO: hasOne has bug with the sourceKey.
// If it is fixed, set foreign key as id in profile.
User.hasOne(Profile);
Profile.belongsTo(User);

User.hasMany(UserActivityLog);
UserActivityLog.belongsTo(User);

User.hasMany(UserLoginHistory);
UserLoginHistory.belongsTo(User);

module.exports = { User, Profile, UserActivityLog, UserLoginHistory };
