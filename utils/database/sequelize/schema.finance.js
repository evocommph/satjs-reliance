const Sequelize = require('sequelize');
const DBManager = require('./manager');
const { User } = require('./schema.common');

const Exchange = DBManager.define('exchanges', {
  exchange_id: { type: Sequelize.STRING(30), allowNull: false, primaryKey: true, field: 'exchange_id' },
  website: { type: Sequelize.STRING, allowNull: false, field: 'website' },
  name: { type: Sequelize.STRING, allowNull: false, field: 'name' },
  data_start: { type: Sequelize.DATEONLY, allowNull: true, field: 'data_start' },
  data_end: { type: Sequelize.DATEONLY, allowNull: true, field: 'data_end' },
  data_quote_start: { type: Sequelize.DATE, allowNull: true, field: 'data_quote_start' },
  data_quote_end: { type: Sequelize.DATE, allowNull: true, field: 'data_quote_end' },
  data_orderbook_start: { type: Sequelize.DATE, allowNull: true, field: 'data_orderbook_start' },
  data_orderbook_end: { type: Sequelize.DATE, allowNull: true, field: 'data_orderbook_end' },
  data_symbols_count: { type: Sequelize.INTEGER, defaultValue: 0, field: 'data_symbols_count' }
}, { underscored: true });

const Asset = DBManager.define('assets', {
  asset_id: { type: Sequelize.STRING(20), allowNull: false, primaryKey: true, field: 'asset_id' },
  name: { type: Sequelize.STRING, allowNull: false, field: 'name' },
  type_is_crypto: { type: Sequelize.INTEGER, allowNull: false, defaultValue: 0, field: 'type_is_crypto' },
  finance_type: { type: Sequelize.STRING(25), allowNull: false, defaultValue: 'crypto', field: 'finance_type' },
  data_start: { type: Sequelize.DATEONLY, allowNull: true, field: 'data_start' },
  data_end: { type: Sequelize.DATEONLY, allowNull: true, field: 'data_end' },
  data_quote_start: { type: Sequelize.DATE, allowNull: true, field: 'data_quote_start' },
  data_quote_end: { type: Sequelize.DATE, allowNull: true, field: 'data_quote_end' },
  data_orderbook_start: { type: Sequelize.DATE, allowNull: true, field: 'data_orderbook_start' },
  data_orderbook_end: { type: Sequelize.DATE, allowNull: true, field: 'data_orderbook_end' },
  data_trade_count: { type: Sequelize.INTEGER, defaultValue: 0, allowNull: true, field: 'data_trade_count' },
  data_symbols_count: { type: Sequelize.INTEGER, defaultValue: 0, allowNull: true, field: 'data_symbols_count' },
  custom_asset: { type: Sequelize.BOOLEAN, allowNull: false, field: 'custom_asset' },
  exchange_code: { type: Sequelize.STRING(25), allowNull: true, field: 'exchange_code' }
}, { indexes: [
  {
    name: 'assets_finance_type',
    fields: ['finance_type']
  }
], underscored: true });

const Symbol = DBManager.define('symbols', {
  symbol_id: { type: Sequelize.STRING(100), allowNull: false, primaryKey: true, field: 'symbol_id' },
  exchange_id: { type: Sequelize.STRING(30), allowNull: false, field: 'exchange_id' },
  symbol_type: { type: Sequelize.STRING(25), allowNull: false, field: 'symbol_type' },
  asset_id_base: { type: Sequelize.STRING(50), allowNull: false, field: 'asset_id_base' },
  asset_id_quote: { type: Sequelize.STRING(50), allowNull: false, field: 'asset_id_quote' },
  future_delivery_time: { type: Sequelize.DATE, allowNull: true, field: 'future_delivery_time' },
  option_type_is_call: { type: Sequelize.BOOLEAN, allowNull: true, field: 'option_type_is_call' },
  option_strike_price: { type: Sequelize.DECIMAL(21,9), allowNull: true, field: 'option_strike_price' },
  option_contract_unit: { type: Sequelize.DECIMAL(21,9), allowNull: true, field: 'option_contract_unit' },
  option_exercise_style: { type: Sequelize.STRING, allowNull: true, field: 'option_exercise_style' },
  option_expiration_time: { type: Sequelize.DATE, allowNull: true, field: 'option_expiration_time' },
  data_start: { type: Sequelize.DATEONLY, allowNull: true, field: 'data_start' },
  data_end: { type: Sequelize.DATEONLY, allowNull: true, field: 'data_end' },
  data_quote_start: { type: Sequelize.DATE, allowNull: true, field: 'data_quote_start' },
  data_quote_end: { type: Sequelize.DATE, allowNull: true, field: 'data_quote_end' },
  data_orderbook_start: { type: Sequelize.DATE, allowNull: true, field: 'data_orderbook_start' },
  data_orderbook_end: { type: Sequelize.DATE, allowNull: true, field: 'data_orderbook_end' }
}, { indexes: [
  {
    name: 'symbols_exchange_id',
    fields: ['exchange_id']
  },
  {
    name: 'symbols_symbol_type',
    fields: ['symbol_type']
  },
  {
    name: 'symbols_asset_id_base_and_quote',
    fields: ['asset_id_base', 'asset_id_quote']
  }
], underscored: true } );

const Rate = DBManager.define('rates', {
  id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true, field: 'id' },
  asset_id: { type: Sequelize.STRING(20), allowNull: false, field: 'asset_id' },
  asset_id_quote: { type: Sequelize.STRING(20), allowNull: false, field: 'asset_id_quote' },
  time: { type: Sequelize.DATE, allowNull: false, defaultValue: Sequelize.NOW, field: 'time' },
  rate: { type: Sequelize.DECIMAL(38,25), allowNull: false, field: 'rate' }
}, { indexes: [
  {
    name: 'rates_asset_id_quote',
    fields: ['asset_id_quote']
  },

  {
    name: 'rates_asset_id_and_quote',
    fields: ['asset_id', 'asset_id_quote']
  }
], underscored: true }  );

const CurrentRate = DBManager.define('current_rates', {
  id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true, field: 'id' },
  asset_id: { type: Sequelize.STRING(20), allowNull: false, field: 'asset_id' },
  asset_id_quote: { type: Sequelize.STRING(20), allowNull: false, field: 'asset_id_quote' },
  time: { type: Sequelize.DATE, allowNull: false, defaultValue: Sequelize.NOW, field: 'time' },
  rate: { type: Sequelize.DECIMAL(38,25), allowNull: false, field: 'rate' },
  previous_time: { type: Sequelize.DATE, allowNull: true, field: 'previous_time' },
  previous_rate: { type: Sequelize.DECIMAL(38,25), allowNull: true, field: 'previous_rate' }
}, { indexes: [
  {
    name: 'current_rates_asset_id_quote',
    fields: ['asset_id_quote']
  },

  {
    name: 'current_rates_asset_id_and_quote',
    fields: ['asset_id', 'asset_id_quote']
  }
], underscored: true }  );

const OHLCV = DBManager.define('ohlcvs', {
  id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true, field: 'id' },
  symbol_id: { type: Sequelize.STRING(100), allowNull: false, field: 'symbol_id' },
  time_period_start: { type: Sequelize.DATE, allowNull: false, field: 'time_period_start' },
  time_period_end: { type: Sequelize.DATE, allowNull: false, field: 'time_period_end' },
  time_open: { type: Sequelize.DATE, allowNull: false, field: 'time_open' },
  time_close: { type: Sequelize.DATE, allowNull: false, field: 'time_close' },
  price_open: { type: Sequelize.DECIMAL(21,9), allowNull: false, field: 'price_open' },
  price_high: { type: Sequelize.DECIMAL(21,9), allowNull: false, field: 'price_high' },
  price_low: { type: Sequelize.DECIMAL(21,9), allowNull: false, field: 'price_low' },
  price_close: { type: Sequelize.DECIMAL(21,9), allowNull: false, field: 'price_close' },
  volume_traded: { type: Sequelize.DECIMAL(21,9), allowNull: false, field: 'volume_traded' },
  trades_count: { type: Sequelize.INTEGER, allowNull: false, field: 'trades_count' }
}, { indexes: [
  {
    name: 'ohlcvs_symbol_id',
    fields: ['symbol_id']
  }
], underscored: true });

const Trade = DBManager.define('trades', {
  id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true, field: 'id' },
  symbol_id: { type: Sequelize.STRING(100), allowNull: false, field: 'symbol_id' },
  time_exchange: { type: Sequelize.DATE, allowNull: false, field: 'time_exchange' },
  time_coinapi: { type: Sequelize.DATE, allowNull: false, field: 'time_coinapi' },
  uuid: { type: Sequelize.STRING(100), allowNull: false, unique: 'trade_uuid', field: 'uuid' },
  price: { type: Sequelize.DECIMAL(21,9), allowNull: false, field: 'price' },
  size: { type: Sequelize.DECIMAL(21,9), allowNull: false, field: 'size' },
  taker_side: { type: Sequelize.STRING(100), allowNull: false, field: 'taker_side' }
}, { indexes: [
  {
    name: 'trades_symbol_id',
    fields: ['symbol_id']
  }
], underscored: true });

const Quote = DBManager.define('quotes', {
  id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true, field: 'id' },
  symbol_id: { type: Sequelize.STRING(100), allowNull: false, field: 'symbol_id' },
  last_trade_id: { type: Sequelize.STRING(100), allowNull: true, field: 'last_trade_id' },
  time_exchange: { type: Sequelize.DATE, allowNull: false, field: 'time_exchange' },
  time_coinapi: { type: Sequelize.DATE, allowNull: false, field: 'time_coinapi' },
  ask_price: { type: Sequelize.DECIMAL(21,9), allowNull: false, field: 'ask_price' },
  ask_size: { type: Sequelize.DECIMAL(21,9), allowNull: false, field: 'ask_size' },
  bid_price: { type: Sequelize.DECIMAL(21,9), allowNull: false, field: 'bid_price' },
  bid_size: { type: Sequelize.DECIMAL(21,9), allowNull: false, field: 'bid_size' }
}, { indexes: [
  {
    name: 'quotes_symbol_id',
    fields: ['symbol_id']
  }
], underscored: true });

const OrderBook = DBManager.define('orderbooks', {
  id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true, field: 'id' },
  symbol_id: { type: Sequelize.STRING(100), allowNull: false, field: 'symbol_id' },
  time_exchange: { type: Sequelize.DATE, allowNull: false, field: 'time_exchange' },
  time_coinapi: { type: Sequelize.DATE, allowNull: false, field: 'time_coinapi' }
}, { indexes: [
  {
    name: 'orderbooks_symbol_id',
    fields: ['symbol_id']
  }
], underscored: true });

const Transaction = DBManager.define('transactions', {
  id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true, field: 'id' },
  orderbook_id: { type: Sequelize.INTEGER, allowNull: false, field: 'orderbook_id' },
  type: { type: Sequelize.STRING(100), allowNull: false, field: 'type' },
  price: { type: Sequelize.DECIMAL(21,9), allowNull: false, field: 'price' },
  size: { type: Sequelize.DECIMAL(21,9), allowNull: false, field: 'size' }
}, { indexes: [
  {
    name: 'transactions_orderbook_id',
    fields: ['orderbook_id']
  }
], underscored: true } );

const UserAssetTransaction = DBManager.define('user_asset_transactions', {
  id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true, field: 'id' },
  action: { type: Sequelize.STRING(20), allowNull: false, field: 'action' },
  quantity: { type: Sequelize.DECIMAL(16,8), allowNull: false, field: 'quantity' },
  sell_price: { type: Sequelize.DECIMAL(30,9), allowNull: true, field: 'sell_price' },
  status: { type: Sequelize.STRING(20), allowNull: false, field: 'status' },
  status_change_unread: { type: Sequelize.BOOLEAN, defaultValue: true, field: 'status_change_unread' },
  status_change_date: { type: Sequelize.DATE, allowNull: true, field: 'status_change_date' }
}, { underscored: true });

const UserAccountPayout = DBManager.define('user_account_payouts', {
  id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true, field: 'id' },
  payout: { type: Sequelize.DECIMAL(30,9), allowNull: false, field: 'payout' },
  credit: { type: Sequelize.BOOLEAN, defaultValue: true, field: 'credit' }
}, { underscored: true });

/**
 * Define Associations HERE
 **/

Symbol.belongsTo(Exchange, { foreignKey: 'exchange_id', as: 'Exchange' });
Exchange.hasMany(Symbol, { foreignKey: 'exchange_id' });

Rate.belongsTo(Asset, { foreignKey: 'asset_id', as: 'Asset' });
Asset.hasMany(Rate, { foreignKey: 'asset_id' });

CurrentRate.belongsTo(Asset, { foreignKey: 'asset_id', as: 'Asset' });
Asset.hasMany(CurrentRate, { foreignKey: 'asset_id' });

OHLCV.belongsTo(Symbol, { foreignKey: 'symbol_id', as: 'Symbol' });
Symbol.hasMany(OHLCV, { foreignKey: 'symbol_id' });

Trade.belongsTo(Symbol, { foreignKey: 'symbol_id', as: 'Symbol' });
Symbol.hasMany(Trade, { foreignKey: 'symbol_id' });

Quote.belongsTo(Symbol, { foreignKey: 'symbol_id', as: 'Symbol' });
Symbol.hasMany(Quote, { foreignKey: 'symbol_id' });
Quote.belongsTo(Trade, { foreignKey: 'last_trade_id', targetKey: 'uuid' });
// Replace this with hasOne if bug is fixed on sequelize see https://github.com/sequelize/sequelize/issues/8105
// Use hasMany as a workAround.
// Quote.hasOne(Trade, { foreignKey: 'last_trade_id', sourceKey: 'uuid' });
Trade.hasMany(Quote, { foreignKey: 'last_trade_id', sourceKey: 'uuid' });

OrderBook.belongsTo(Symbol, { foreignKey: 'symbol_id', as: 'Symbol' });
Symbol.hasMany(OrderBook, { foreignKey: 'symbol_id' });

Transaction.belongsTo(OrderBook, { foreignKey: 'orderbook_id', as: 'OrderBook' });
OrderBook.hasMany(Transaction, { foreignKey: 'orderbook_id' });

User.hasMany(UserAssetTransaction);
UserAssetTransaction.belongsTo(User);

Rate.hasMany(UserAssetTransaction);
UserAssetTransaction.belongsTo(Rate);
//Asset.hasMany(UserAssetTransaction, { foreignKey: 'asset_id_quote', as: 'AssetIdQuote' } );

User.hasMany(UserAccountPayout);
UserAccountPayout.belongsTo(User);

module.exports = {
  Exchange, Asset, Symbol, Rate, CurrentRate,
  OHLCV, Trade, Quote, OrderBook, Transaction,
  UserAssetTransaction, UserAccountPayout
};
