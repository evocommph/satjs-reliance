const Manager = require('./manager');
const { User, Profile, UserLoginHistory, UserActivityLog } = require('./schema.common');
const {
  Exchange, Asset, Symbol, Rate, CurrentRate,
  OHLCV, Trade, Quote, OrderBook, Transaction,
  UserAssetTransaction, UserAccountPayout
} = require('./schema.finance');

module.exports = {
  Manager,
  Schema : {
    Exchange, Asset, Symbol, Rate, CurrentRate, OHLCV, Trade, Quote, OrderBook, Transaction,
    User, Profile, UserLoginHistory, UserActivityLog, UserAssetTransaction, UserAccountPayout
  }
};
