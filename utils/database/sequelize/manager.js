const Config = require('config');
const Sequelize = require('sequelize');

const database = Config.database;
const dbmanager = new Sequelize(database.db, database.username, database.password, {
  host: database.host | 'localhost',
  port: database.port | 3306,
  dialect: 'mysql',
  pool: database.pool,
  operatorsAliases: Sequelize.Op,
  logging: database.debug ? console.log : false,
  dialectOptions: {
    charset: 'utf8mb4',
    collate: 'utf8mb4_unicode_ci'
  }
});

module.exports = dbmanager;
