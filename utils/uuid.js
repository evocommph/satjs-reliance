const Config = require('config');

class GeneratorID {
  static Timestamp() {
    return Date.now().toString();
  }

  static Random(uuid) {
    return uuid();
  }

  static Namespace(uuid) {
    return uuid(Config.security.uuid.namespace, uuid[Config.security.uuid.type]);
  }
}

let generator = GeneratorID.Timestamp;
if (Config.security.uuid.version === 'v1' || Config.security.uuid.version === 'v4') {
  const uuid = require(`uuid/${Config.security.uuid.version}`);
  generator = GeneratorID.Random.bind(null, uuid);
} else if (Config.security.uuid.version === 'v3' || Config.security.uuid.version === 'v5') {
  const uuid = require(`uuid/${Config.security.uuid.version}`);
  generator = GeneratorID.Namespace.bind(null, uuid);
}

module.exports = generator;
