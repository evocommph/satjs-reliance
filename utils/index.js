const uuid = require('./uuid');
const logger = require('./logger');

module.exports = { uuid, logger };
