const Config = require('config');
const Winston = require('winston');
const { ensureFileSync } = require('fs-extra');

const simpleFormatter = function (options) {
  const header = `${options.timestamp()} [${options.level.toUpperCase().padEnd(7)}]:`;
  const message = options.message ? options.message : '';
  const meta = options.meta && Object.keys(options.meta).length ? `\n\tmeta: ${JSON.stringify(options.meta)}` : '';
  return `${header}\t${message} ${meta}`;
};

const colorizeFormatter = function (options) {
  const level = `[${Winston.config.colorize(options.level, options.level.toUpperCase().padEnd(7))}]:`;
  const header = `${options.timestamp()} ${level}`;
  const message = options.message ? options.message : '';
  const meta = options.meta && Object.keys(options.meta).length ? `\n\tmeta: ${JSON.stringify(options.meta)}` : '';
  return `${header}\t${message} ${meta}`;
};

const logTransportFile = new Winston.transports.File({
  filename: Config.logger.file,
  timestamp: () => new Date().toISOString(),
  formatter: simpleFormatter,
  json: false
});

const logTransportConsole = new Winston.transports.Console({
  timestamp: () => new Date().toISOString(),
  formatter: colorizeFormatter,
  json: false
});

const logger = new Winston.Logger({
  level: Config.logger.level,
  levels: Winston.config.syslog.levels,
  transports: [logTransportFile, logTransportConsole]
});

if (!Config.logger.console) {
  logger.remove(Winston.transports.Console);
}

// Winston is having problem if logger path don't exists
// Remove file transport is file is unaccessible
if (logger.transports.file) {
  try {
    ensureFileSync(Config.logger.file);
  } catch (err) {
    logger.remove(Winston.transports.File);
  }
}

module.exports = logger;
