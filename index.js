const Http = require('http');
const Https = require('https');
const RedirectHttps = require('redirect-https');
const Config = require('config');
const GreenLock = require('greenlock-hapi');
const Server = require('./server');

const manifest = {
  server: {
    port: Config.server.port,
    host: Config.server.host,
    routes: {
      cors: { origin: Config.server.allowOrigins }
    }
  },
  register: {
    plugins: [
      // HapiJS Plugins
      { plugin: require('hapi-auth-basic') },
      { plugin: require('hapi-auth-bearer-token') },
      { plugin: require('hapi-acl-auth'),
        options: {
          handler: (request) => {
            return request.auth.credentials;
          }
        }
      },

      // Custom Plugins
      { plugin: './plugins/logger' },
      { plugin: './plugins/users' },
      { plugin: './plugins/finance' }
    ]
  }
};

const options = {
  relativeTo: __dirname
};

if (!Config.server.httpOnly) {
  const le = GreenLock.create({
    server: Config.server.staging ? 'staging' : 'https://acme-v01.api.letsencrypt.org/directory',
    approveDomains: function (opts, certs, cb) { // eslint-disable-line object-shorthand
      if (certs) {
        opts.domains = certs.altnames;
      } else {
        opts.email = Config.server.ssl.sslEmail;
        opts.agreeTos = true;
      }

      // Check if domains matches with our fqdn
      if (typeof opts.domains === 'string' && opts.domains === Config.server.fqdn) {
        cb(null, { certs, options: opts });
      } else if (Array.isArray(opts.domains) && opts.domains.indexOf(Config.server.fqdn) > -1) {
        cb(null, { certs, options: opts });
      } else {
        cb(new Error('Domain not supported'));
      }
    },
    debug: false
  });

  const acmeResponder = le.middleware();
  const httpsServer = Https.createServer(le.httpsOptions).listen(443, Config.server.host);

  Http.createServer(le.middleware(RedirectHttps())).listen(80, Config.server.host, () => {
    // TODO:
    // Log here.
  });

  // Add letsencrypt plugin
  manifest.register.plugins.push( {
    plugin: './plugins/letsencrypt',
    options: { handler: acmeResponder }
  } );

  // Update hapijs options
  options.listener = httpsServer;
  options.tls = true;
  options.autoListen = false;
}

Server.configure(manifest, options);
Server.start();
